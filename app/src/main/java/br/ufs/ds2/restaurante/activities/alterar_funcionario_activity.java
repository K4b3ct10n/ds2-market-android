package br.ufs.ds2.restaurante.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.controllers.AppController;

import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class alterar_funcionario_activity extends AppCompatActivity {
    private String URL_UPDATE_FUNCIONARIO = "http://rest-dsdois.rhcloud.com/update_garcom.php?";
    private String URL_UPDATE_GARCOM = "http://rest-dsdois.rhcloud.com/update_garcom.php?";
    private String URL_UPDATE_GERENTE = "http://rest-dsdois.rhcloud.com/update_gerente.php?";
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alterar_funcionario_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar_principal);
        toolbar.setTitle("Atualizar Funcionário");
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();

        EditText nome = (EditText) findViewById(R.id.f_nome);
        EditText cpf = (EditText) findViewById(R.id.f_cpf);
        EditText endereco = (EditText) findViewById(R.id.f_endereco);
        EditText telefone = (EditText) findViewById(R.id.f_telefone);
        EditText salario = (EditText) findViewById(R.id.f_salario);
        EditText dataAdmissao = (EditText) findViewById(R.id.f_dataAdmissao);
        EditText dataDemissao = (EditText) findViewById(R.id.f_dataDemissao);
        EditText cargo = (EditText) findViewById(R.id.f_cargo);

        nome.setText(bd.getString("nome"));
        cpf.setText(bd.getString("cpf"));
        endereco.setText(bd.getString("endereco"));
        telefone.setText(bd.getString("telefone"));
        salario.setText(String.valueOf(bd.getDouble("salario")));
        Log.e("DATA DE ADMISSÃO E D: ",bd.getString("DataAdmissao") + " " +bd.getString("DataDemissao"));
        dataAdmissao.setText(bd.getString("DataAdmissao"));
        dataDemissao.setText(bd.getString("DataDemissao"));
        cargo.setText(bd.getString("cargo"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void salvar(View v) throws URISyntaxException {
        EditText nome = (EditText) findViewById(R.id.f_nome);
        EditText cpf = (EditText) findViewById(R.id.f_cpf);
        EditText endereco = (EditText) findViewById(R.id.f_endereco);
        EditText telefone = (EditText) findViewById(R.id.f_telefone);
        EditText salario = (EditText) findViewById(R.id.f_salario);
        EditText dataAdmissao = (EditText) findViewById(R.id.f_dataAdmissao);
        EditText dataDemissao = (EditText) findViewById(R.id.f_dataDemissao);
        EditText cargo = (EditText) findViewById(R.id.f_cargo);

        if (cargo.getText().toString().equals("gerente")){URL_UPDATE_FUNCIONARIO = URL_UPDATE_GERENTE;}
        URIBuilder uri = new URIBuilder(URL_UPDATE_FUNCIONARIO);
        uri.addParameter("nome",nome.getText().toString());
        uri.addParameter("cpf",cpf.getText().toString());
        uri.addParameter("endereco",endereco.getText().toString());
        uri.addParameter("telefone",telefone.getText().toString());
        uri.addParameter("salario",salario.getText().toString());
        uri.addParameter("dataDeAdmissao",dataAdmissao.getText().toString());
        uri.addParameter("dataDeDemissao",dataDemissao.getText().toString());
        URL_UPDATE_FUNCIONARIO = uri.build().toString();

        Log.e("salvarFuncionario",URL_UPDATE_FUNCIONARIO);
        StringRequest req = new StringRequest(Request.Method.GET, URL_UPDATE_FUNCIONARIO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("ok")){
                            Log.e("HttpClient", "Funcionario atualizado: " + response);
                            Toast t = Toast.makeText(getApplicationContext(),"FUNCIONÁRIO ATUALIZADO COM SUCESSO",Toast.LENGTH_LONG);
                            t.show();
                            Intent intent = new Intent(getApplicationContext(),FuncionarioActivity.class);
                            startActivity(intent);
                        }else{
                            Log.e("HttpClient", "error: ");
                            Toast t = Toast.makeText(getApplicationContext(),"ERRO, TENTE OUTRA VEZ.",Toast.LENGTH_LONG);
                            t.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("HttpClient", "error: " + error.toString());
                        Toast t = Toast.makeText(getApplicationContext(),"ERRO, TENTE OUTRA VEZ.",Toast.LENGTH_LONG);
                        t.show();
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req);
    }
}
