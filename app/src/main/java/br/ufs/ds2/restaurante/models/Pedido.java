package br.ufs.ds2.restaurante.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Junior on 10/11/2015.
 */
public class Pedido implements Parcelable {

    private int id;
    private int numPedido;
    private Refeicao refeicao;
    private int  idComanda;


    public Pedido(int id, int numPedido, Refeicao refeicao, int idComanda){
        this.id = id;
        this.refeicao = refeicao;
        this.numPedido = numPedido;
        this.idComanda = idComanda;
    }

    public Pedido(int id, Refeicao refeicao){
        this.id = id;
        this.refeicao = refeicao;
    }

    public Pedido(Refeicao refeicao){
        this.refeicao = refeicao;
    }

    protected Pedido(Parcel in) {
        id = in.readInt();
        refeicao = (Refeicao) in.readValue(Refeicao.class.getClassLoader());
    }

    public static final Creator<Pedido> CREATOR = new Creator<Pedido>() {
        @Override
        public Pedido createFromParcel(Parcel in) {
            return new Pedido(in);
        }

        @Override
        public Pedido[] newArray(int size) {
            return new Pedido[size];
        }
    };

    public int getId() {
        return id;
    }

    public Refeicao getRefeicao() {
        return refeicao;
    }

    public void setRefeicao(Refeicao refeicao) {
        this.refeicao = refeicao;
    }

    public int getNumPedido() {
        return numPedido;
    }

    public void setNumPedido(int numPedido) {
        this.numPedido = numPedido;
    }

    public void clone(Pedido pedido){
        this.id = pedido.getId();
        this.numPedido = pedido.getNumPedido();
        this.refeicao = pedido.getRefeicao();
        this.idComanda = pedido.getIdComanda();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeValue(refeicao);
    }

    public int getIdComanda() {
        return idComanda;
    }

    public void setIdComanda(int idComanda) {
        this.idComanda = idComanda;
    }
}
