package br.ufs.ds2.restaurante.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Junior on 11/11/2015.
 */
public class Garçom extends Funcionário implements Parcelable{

    public Garçom(String nome, String cpf, String endereco, String telefone, Float salario){
        super.setNome(nome);
        super.setCpf(cpf);
        super.setEndereço(endereco);
        super.setTelefone(telefone);
        super.setSalario(salario);
    }

    public Garçom(String nome, String cpf, String endereco, String telefone){
        super.setNome(nome);
        super.setCpf(cpf);
        super.setEndereço(endereco);
        super.setTelefone(telefone);
    }

    protected Garçom(Parcel in) {
        super.setNome(in.readString());
        super.setCpf(in.readString());
        super.setEndereço(in.readString());
        super.setTelefone(in.readString());
        super.setSalario(in.readFloat());
    }

    public static final Creator<Garçom> CREATOR = new Creator<Garçom>() {
        @Override
        public Garçom createFromParcel(Parcel in) {
            return new Garçom(in);
        }

        @Override
        public Garçom[] newArray(int size) {
            return new Garçom[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(super.getNome());
        dest.writeString(super.getCpf());
        dest.writeString(super.getEndereço());
        dest.writeString(super.getTelefone());
        dest.writeFloat(super.getSalario());
    }

    @Override
    public int getTipo() {
        return 0;
    }
}
