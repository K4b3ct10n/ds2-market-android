package br.ufs.ds2.restaurante.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.adapters.GanhosAdapter;
import br.ufs.ds2.restaurante.adapters.GastosAdapter;
import br.ufs.ds2.restaurante.controllers.AppController;
import br.ufs.ds2.restaurante.settings.RestauranteSettings;

/**
 * Created by Junior on 10/11/2015.
 */
public class GanhosFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    /*
    * Classe responsável por realizar o preenchimento dos fragmentos dos ganhos
    * */

    private RecyclerView recyclerView;
    private GanhosAdapter adapter;
    private String dataInicial;//Data passada como parâmetro da activity DataActivity
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Float> ganhos; //Lista de ganhos
    // SwypeView
    private SwipeRefreshLayout swipeRefreshLayout;

    //Acessa a lista
    public ArrayList<Float> getGanhos() {
        return ganhos;
    }
    //Adiciona na lista
    public void adicionarGanhos(Float ganho) {
        this.ganhos.add(ganho);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ganhos = new ArrayList<>();

        //Preenchendo o cardview com os ganhos
        preencherLista();
        adapter = new GanhosAdapter(ganhos, getFragmentManager());
    }

    //Construtor para passagem de parãmetro da data inicial, atribuída pelo usuário, para realização da consulta
    public GanhosFragment(String data) {
        this.dataInicial = data;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_fornecedor, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.listFornecedor);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        // SwipeViewRefresh Listener
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        //dataInicial = getArguments().getString("dataInicial");

        return view;
    }


    //Método responsável por realizar as consultas via JSON e/ou web service
    private void preencherLista(){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, RestauranteSettings.getGanhos(dataInicial)
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray arrayJson = null;
                try {
                    arrayJson = response.getJSONArray("ganhos");
                    for (int i=0; i<arrayJson.length();i++) {
                        JSONObject forn = (JSONObject) arrayJson.get(i);
                        ganhos.add(new Float(forn.getString("valor")));
                        adapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    Log.e("FornecedorFragment", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Consulta vazia para esta data!", Toast.LENGTH_LONG).show();
            }
        });
        // Adicionando requisição a fila de requisições
        AppController.getInstance().addToRequestQueue(request);
    }


    //Métodos auxiliares
    @Override
    public void onRefresh() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        adapter.notifyDataSetChanged();

    }
}
