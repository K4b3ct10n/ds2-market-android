package br.ufs.ds2.restaurante.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import br.ufs.ds2.restaurante.R;

public class DataActivity extends AppCompatActivity  {

    /*
        Classe responsável por fazer o preenchimento da data para relização das consultas
    */

    private String dateFormat; //String resposável por pegar a data e passar como parâmetro para outra classe
                                //É o tipo string por causa da consulta que é realizada com string
    //Passa a data informada pelo usuário
    public final static String EXTRA_MESSAGE = "dataInicial"; //Uso obrigatório para passar parâmetro para outra Activity
    //Data informada pelo usuário para realizar as consultas
    private DatePicker data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        //Botão responsável por fazer a comunicação e consultas
        Button buttonData = (Button) findViewById(R.id.buttonRelatorio);

        buttonData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Atribuindo o valor informado pelo usuário
                data = (DatePicker) findViewById(R.id.dataInicio);
                //Formatando a data e pegando o ano, o mês e o dia, somou +1 porque o mês começa de 0 e a consulta começa de 1
                dateFormat = data.getYear()+"-"+(data.getMonth()+1)+"-"+data.getDayOfMonth();

                Intent intent = new Intent(DataActivity.this, RelatorioActivity.class);
                //Passando o atributo data como parâmetro para próxima activity
                intent.putExtra(EXTRA_MESSAGE, dateFormat);
                startActivity(intent);

                finish();
            }
        });

    }

    //Métodos auxiliares para exibição da Activity
    public DataActivity(){}
    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
    }

    @Override
    public void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
    }


}
