package br.ufs.ds2.restaurante.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.controllers.AppController;

public class AlterarRefeicaoActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private String URL_UPDATE_REFEICAO = "http://rest-dsdois.rhcloud.com/update_refeicao.php?";
    private int ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alterar_refeicao);
        toolbar = (Toolbar) findViewById(R.id.toolbar_principal);
        toolbar.setTitle("Atualizar Refeição");
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        EditText nome = (EditText) findViewById(R.id.at_nome);
        EditText preco = (EditText) findViewById(R.id.at_preco);
        EditText desconto = (EditText) findViewById(R.id.at_desconto);
        EditText descricao = (EditText) findViewById(R.id.at_descricao);
        EditText quantidade = (EditText) findViewById(R.id.at_quantidade);
        EditText valorDeCompra = (EditText) findViewById(R.id.at_valorDeCompra);
        nome.setText(bd.getString("nome"));
        preco.setText(String.valueOf(bd.getFloat("preco")));
        desconto.setText( String.valueOf(bd.getFloat("desconto")) );
        descricao.setText(bd.getString("descricao"));
        quantidade.setText(String.valueOf(bd.getInt("quantidade")));
        valorDeCompra.setText(String.valueOf(bd.getDouble("valorDeCompra")));
        ID = bd.getInt("id");

        
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void salvar(View v) throws URISyntaxException {
        EditText nome = (EditText) findViewById(R.id.at_nome);
        EditText preco = (EditText) findViewById(R.id.at_preco);
        EditText desconto = (EditText) findViewById(R.id.at_desconto);
        EditText descricao = (EditText) findViewById(R.id.at_descricao);
        EditText quantidade = (EditText) findViewById(R.id.at_quantidade);
        EditText valorDeCompra = (EditText) findViewById(R.id.at_valorDeCompra);
        /*URL_UPDATE_REFEICAO += "id_refeicao="+ID+"&nome=" + nome.getText() + "&preco=" + preco.getText() +
                "&desconto=" + desconto.getText()+"&quantidade="+quantidade.getText() +
                "&valorDeCompra="+valorDeCompra.getText();*/
        URIBuilder uri = new URIBuilder(URL_UPDATE_REFEICAO);
        uri.addParameter("id_refeicao",String.valueOf(ID))
                .addParameter("nome",nome.getText().toString())
                .addParameter("preco",preco.getText().toString())
                .addParameter("desconto",desconto.getText().toString())
                .addParameter("descricao",descricao.getText().toString())
                .addParameter("quantidade",quantidade.getText().toString())
                .addParameter("valorDeCompra",valorDeCompra.getText().toString())
                .build();
        URL_UPDATE_REFEICAO = uri.toString();
        //URL_UPDATE_REFEICAO = URL_UPDATE_REFEICAO.replaceAll("\\s+","_");
        Log.e("salvarRefeicao", "URL: " + URL_UPDATE_REFEICAO );
        StringRequest req = new StringRequest(Request.Method.GET, URL_UPDATE_REFEICAO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("ok")){
                            Log.e("HttpClient", "success! response: " + response);
                            Toast t = Toast.makeText(getApplicationContext(),"REFEIÇÃO ATUALIZADA COM SUCESSO",Toast.LENGTH_LONG);
                            t.show();
                            Intent intent = new Intent(getApplicationContext(),FuncionarioActivity.class);
                            startActivity(intent);
                        }else{
                            Log.e("HttpClient", "error: ");
                            Toast t = Toast.makeText(getApplicationContext(),"ERRO, TENTE OUTRA VEZ.",Toast.LENGTH_LONG);
                            t.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("HttpClient", "error: " + error.toString());
                        Toast t = Toast.makeText(getApplicationContext(),"ERRO, TENTE OUTRA VEZ.",Toast.LENGTH_LONG);
                        t.show();
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req);
    }
}
