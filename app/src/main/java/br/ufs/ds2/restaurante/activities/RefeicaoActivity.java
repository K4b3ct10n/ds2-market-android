package br.ufs.ds2.restaurante.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.adapters.AdapterRefeicao;
import br.ufs.ds2.restaurante.controllers.AppController;
import br.ufs.ds2.restaurante.models.Refeicao;

public class RefeicaoActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
    private List<Refeicao> lista_refeicao;
    private RecyclerView rv_refeicao;
    private RecyclerView.Adapter rv_adapter;
    private RecyclerView.LayoutManager rv_lm;
    private Toolbar toolbar;
    static String URL_LIST_REFEICOES = "http://rest-dsdois.rhcloud.com/read_refeicao.php";
    static String URL_DELETE_REFEICAO = "http://rest-dsdois.rhcloud.com/delete_refeicao.php?id_refeicao=";

    private SwipeRefreshLayout swipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refeicao);
        toolbar = (Toolbar) findViewById(R.id.toolbar_principal);
        toolbar.setTitle("Refeições");
        setSupportActionBar(toolbar);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.sp_refeicao);
        swipeRefreshLayout.setOnRefreshListener(this);
        lista_refeicao = new ArrayList<Refeicao>();
        rv_refeicao = (RecyclerView) findViewById(R.id.recycler_view_refeicao);
        rv_refeicao.setHasFixedSize(true);
        rv_adapter = new AdapterRefeicao(lista_refeicao,getApplicationContext());
        rv_lm = new LinearLayoutManager(this);
        rv_refeicao.setLayoutManager(rv_lm);
        getRefeicoes();
        rv_refeicao.setAdapter(rv_adapter);
    }


    @Override
    public void onRefresh() {
        lista_refeicao.clear();
        rv_adapter.notifyDataSetChanged();
        getRefeicoes();
        swipeRefreshLayout.setRefreshing(false);
    }

    public void getRefeicoes(int n){
        for(int i=0; i < n; i++){
            lista_refeicao.add(new Refeicao("Refeicao"+i, (float) 10.54));
        }
    }


    public void loadRefeicoes(JSONObject response){
        try {
            JSONArray perguntas = response.getJSONArray("refeicoes");
            for(int i=0; i < perguntas.length(); i++){
                JSONObject pg = perguntas.getJSONObject(i);
                Refeicao refeicao = new Refeicao(pg.getString("nome"), (float) pg.getDouble("preco"));
                refeicao.setId(pg.getInt("id_refeicao"));
                refeicao.setDescricao(pg.getString("descricao"));
                refeicao.setQuantidade(pg.getInt("quantidade"));
                refeicao.setDesconto((float)pg.getDouble("desconto"));
                refeicao.setValorDeCompra(pg.getDouble("valorDeCompra"));
                lista_refeicao.add(refeicao);
                rv_adapter.notifyItemInserted(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void excluirRefeicao(int id, final int position){
        Log.e("excluirRefeicao","URL: "+URL_DELETE_REFEICAO+id);
        StringRequest req = new StringRequest(Request.Method.GET, URL_DELETE_REFEICAO+id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response);
                        if (response.contains("ok")){
                            Toast t = Toast.makeText(getApplicationContext(),"REFEIÇÃO EXCLUÍDA COM SUCESSO",Toast.LENGTH_SHORT);
                            t.show();
                            lista_refeicao.remove(position);
                            rv_adapter.notifyItemRemoved(position);
                            for(int i=position; i < lista_refeicao.size(); i++){
                                rv_adapter.notifyItemChanged(i);
                            }
                        }else{
                            Toast t = Toast.makeText(getApplicationContext(),"ESSA REFEIÇÃO NÃO PODE SER EXCLUÍDA NO MOMENTO!",Toast.LENGTH_SHORT);
                            t.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("HttpClient", "error: " + error.toString());
                        Toast t = Toast.makeText(getApplicationContext(),"ERRO, TENTE OUTRA VEZ.",Toast.LENGTH_LONG);
                        t.show();
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req);
    }

    private void getRefeicoes(){
        JsonObjectRequest newsReq = new JsonObjectRequest(Request.Method.GET, URL_LIST_REFEICOES,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("getRefeicoes ", response.toString());
                        loadRefeicoes(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("getFuncionarios ", "Erro: " + error.getMessage());
            }
        }) {
            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(newsReq);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onContextItemSelected(MenuItem item) {
        int position = item.getOrder();
        switch (item.getItemId()) {
            case R.id.call:
                //Toast.makeText(getApplicationContext(),"ID: " + String.valueOf(item.getOrder()) ,Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(),AlterarRefeicaoActivity.class);
                intent.putExtra("id", lista_refeicao.get(position).getId());
                intent.putExtra("nome",lista_refeicao.get(position).getNome());
                intent.putExtra("descricao",lista_refeicao.get(position).getDescricao());
                intent.putExtra("preco",lista_refeicao.get(position).getPreço());
                intent.putExtra("desconto",lista_refeicao.get(position).getDesconto());
                intent.putExtra("quantidade",lista_refeicao.get(position).getQuantidade());
                intent.putExtra("valorDeCompra",lista_refeicao.get(position).getValorDeCompra());
                startActivity(intent);
                break;
            case R.id.msg:
                excluirRefeicao(lista_refeicao.get(position).getId(),position);
                break;
        }
        return super.onContextItemSelected(item);
    }

}
