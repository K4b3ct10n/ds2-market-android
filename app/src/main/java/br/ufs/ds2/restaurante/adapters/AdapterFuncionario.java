package br.ufs.ds2.restaurante.adapters;
/**
 * Created by Desconhecido on 10/11/2015.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.models.Funcionario;

public class AdapterFuncionario extends RecyclerView.Adapter<AdapterFuncionario.ViewHolder> {
    private List<Funcionario> lista_funcionario;
    private LayoutInflater layoutInflater;
    static Context contexto;
    ContextMenu.ContextMenuInfo info;


    public AdapterFuncionario(List<Funcionario> listFuncionario, Context contexto){
        this.contexto = contexto;
        lista_funcionario = listFuncionario;
        layoutInflater = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public AdapterFuncionario(){}

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.view_funcionario,viewGroup,false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.imgview_foto.setImageResource(R.drawable.account_circle);
        viewHolder.txtview_nome.setText(lista_funcionario.get(i).getNome());
        viewHolder.txtview_cargo.setText(lista_funcionario.get(i).getCargo());
        viewHolder.position = i;
        Log.d("onBindViewHolder",lista_funcionario.get(i).getNome());
    }

    @Override
    public int getItemCount() {
        return lista_funcionario.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener, View.OnCreateContextMenuListener{
        ImageView imgview_foto;
        TextView txtview_nome;
        TextView txtview_cargo;
        int position;

        public ViewHolder(View v){
            super(v);
            imgview_foto = (ImageView) v.findViewById(R.id.imgview_foto_funcionario);
            txtview_nome = (TextView) v.findViewById(R.id.txtview_nome_funcionario);
            txtview_cargo = (TextView) v.findViewById(R.id.txtview_cargo_funcionario);
            v.setOnClickListener(this);
            v.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            new AdapterFuncionario().info = menuInfo;
            menu.setHeaderTitle("Operações");
            menu.add(0, R.id.call,position, "Alterar");//groupId, itemId, order, title
            menu.add(0, R.id.msg, position, "Excluir");
        }

    }
}
