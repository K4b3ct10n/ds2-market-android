package br.ufs.ds2.restaurante.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.controllers.AppController;

public class AlterarFornecedorActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private String URL_UPDATE_FORNECEDOR = "http://rest-dsdois.rhcloud.com/update_fornecedor.php?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alterar_fornecedor);
        toolbar = (Toolbar) findViewById(R.id.toolbar_principal);
        toolbar.setTitle("Atualizar Fornecedor");
        setSupportActionBar(toolbar);
        EditText nome = (EditText) findViewById(R.id.at_forn_nome);
        EditText endereco = (EditText) findViewById(R.id.at_forn_endereco);
        EditText cnpj = (EditText) findViewById(R.id.at_forn_cnpj);
        EditText telefone = (EditText) findViewById(R.id.at_forn_telefone);
        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        nome.setText(bd.getString("nome"));
        endereco.setText(bd.getString("endereco"));
        cnpj.setText(bd.getString("cnpj"));
        telefone.setText(bd.getString("telefone"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void atualizar(View v) throws URISyntaxException {
        EditText nome = (EditText) findViewById(R.id.at_forn_nome);
        EditText endereco = (EditText) findViewById(R.id.at_forn_endereco);
        EditText cnpj = (EditText) findViewById(R.id.at_forn_cnpj);
        EditText telefone = (EditText) findViewById(R.id.at_forn_telefone);

        URIBuilder uri = new URIBuilder(URL_UPDATE_FORNECEDOR);
        uri.addParameter("nome",nome.getText().toString());
        uri.addParameter("endereco",endereco.getText().toString());
        uri.addParameter("cnpj",cnpj.getText().toString());
        uri.addParameter("telefone",telefone.getText().toString());
        String url = uri.build().toString();

        Log.e("ATUALIZAR FORNECEDOR: ", url);
        //url = url.replaceAll("\\s+","_");
        StringRequest req = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("ok")) {
                            Log.e("HttpClient", "ATUALIZAR FORNECEDOR: <> " + response);
                            Toast t = Toast.makeText(getApplicationContext(), "FORNECEDOR ATUALIZADO COM SUCESSO", Toast.LENGTH_LONG);
                            t.show();
                            Intent intent = new Intent(getApplicationContext(), FuncionarioActivity.class);
                            startActivity(intent);
                        }else{
                            Log.e("HttpClient", "error: ");
                            Toast t = Toast.makeText(getApplicationContext(),"ERRO, TENTE OUTRA VEZ.",Toast.LENGTH_LONG);
                            t.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("HttpClient", "error: " + error.toString());
                        Toast t = Toast.makeText(getApplicationContext(),"ERRO, TENTE OUTRA VEZ.",Toast.LENGTH_LONG);
                        t.show();
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req);
    }
}
