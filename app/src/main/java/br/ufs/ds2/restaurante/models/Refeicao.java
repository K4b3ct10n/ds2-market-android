package br.ufs.ds2.restaurante.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Junior on 12/11/2015.
 */
public class Refeicao implements Parcelable {
    private int id;
    private String nome;
    private String descricao;
    private float preço;
    private float desconto;
    private int quantidade;
    private double valorDeCompra;

    public Refeicao(int id, String nome, String descricao, float preço, float desconto, int quantidade) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.preço = preço;
        this.desconto = desconto;
        this.quantidade = quantidade;
    }

    public Refeicao(String nome, float preço){
        this.nome = nome;
        this.preço = preço;
    }

    public Refeicao(int id, String nome, float preço){
        this.id = id;
        this.nome = nome;
        this.preço = preço;
    }

    public Refeicao(int id, String nome, String descricao, float preço){
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.preço = preço;
        this.desconto = 0;
        this.quantidade = 0;
    }

    protected Refeicao(Parcel in) {
        id = in.readInt();
        nome = in.readString();
        descricao = in.readString();
        preço = in.readFloat();
        desconto = in.readFloat();
        quantidade = in.readInt();
    }

    public static final Creator<Refeicao> CREATOR = new Creator<Refeicao>() {
        @Override
        public Refeicao createFromParcel(Parcel in) {
            return new Refeicao(in);
        }

        @Override
        public Refeicao[] newArray(int size) {
            return new Refeicao[size];
        }
    };

    public double getValorDeCompra(){return this.valorDeCompra;}

    public void setValorDeCompra(double valor){this.valorDeCompra = valor;}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public float getPreço() {
        return preço;
    }

    public void setPreço(float preço) {
        this.preço = preço;
    }

    public float getDesconto() {
        return desconto;
    }

    public void setDesconto(float desconto) {
        this.desconto = desconto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nome);
        dest.writeString(descricao);
        dest.writeFloat(preço);
        dest.writeFloat(desconto);
        dest.writeInt(quantidade);
    }

    public void clone(Refeicao refeicao){
        this.id = refeicao.getId();
        this.nome = refeicao.getNome();
        this.descricao = refeicao.getDescricao();
        this.preço = refeicao.getPreço();
        this.desconto = refeicao.getDesconto();
        this.quantidade = refeicao.getQuantidade();
    }
}
