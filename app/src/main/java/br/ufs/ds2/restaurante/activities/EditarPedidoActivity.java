package br.ufs.ds2.restaurante.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.fragments.PedidosFragment;
import br.ufs.ds2.restaurante.models.Pedido;
import br.ufs.ds2.restaurante.models.Refeicao;

public class EditarPedidoActivity extends AppCompatActivity {

    private Pedido pedido;
    private Toolbar mToolbar;
    List<Refeicao> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_pedido);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle != null && bundle.containsKey("idPedido")){
            int idPedido = bundle.getInt("idPedido");
            for(Pedido p :PedidosFragment.mList){
                if(p.getId() == idPedido){
                    pedido = p;
                    break;
                }
            }
        }

        mToolbar = (Toolbar) findViewById(R.id.tb);
        mToolbar.setTitle("Editar Pedido");
        mToolbar.setSubtitle(pedido.getNumPedido());
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        preencherLista();

        Spinner dropdown = (Spinner)findViewById(R.id.spinner1);
        mList = new ArrayList<>();
        mList.add(new Refeicao(1, "Refeição 1", 100));
        mList.add(new Refeicao(2, "Refeição 2", 200));
        mList.add(new Refeicao(3, "Refeição 3", 300));


        String[] items = new String[]{"Refeição 1", "Refeição 2", "Refeição 3"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);
    }

    public void editarPedido(View view) {
        EditText txtNumPedido = (EditText) findViewById(R.id.txtNumPedido);
        EditText txtNomePedido = (EditText) findViewById(R.id.txtNomePedido);
        Spinner dropdown = (Spinner)findViewById(R.id.spinner1);
        String nomeRefeicao = dropdown.getSelectedItem().toString();

        for(Refeicao refeicao: mList){
            if(refeicao.getNome().equals(nomeRefeicao)){
                pedido.setRefeicao(refeicao);
                break;
            }
        }
        //pedido.setNomePedido(txtNomePedido.getText().toString());
        pedido.setNumPedido(Integer.parseInt(txtNumPedido.getText().toString()));

        Toast.makeText(this, "Editado com Sucesso!", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void preencherLista(){
        EditText txtNumPedido = (EditText) findViewById(R.id.txtNumPedido);
        EditText txtNomePedido = (EditText) findViewById(R.id.txtNomePedido);

        txtNumPedido.setText(pedido.getNumPedido()+getString(R.string.stringEmpty));
        //txtNomePedido.setText(pedido.getNomePedido());
    }
}
