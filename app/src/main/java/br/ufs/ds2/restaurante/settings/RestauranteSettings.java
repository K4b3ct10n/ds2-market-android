package br.ufs.ds2.restaurante.settings;

import android.text.Editable;

import java.util.Date;

/**
 * Created by Junior on 16/11/2015.
 * Classe que define todas as URLs de requisições ao webservice
 */
public class RestauranteSettings {

    private static final String consultar_funcionarios = "http://rest-dsdois.rhcloud.com/read_funcionarios.php";

    private static final String criar_comanda = "http://rest-dsdois.rhcloud.com/create_comanda.php?cliente=%1$s&garcom=%2$s&mesa=%3$s&data_hora=%4$s";
    private static final String remover_comanda = "http://rest-dsdois.rhcloud.com/delete_comanda.php?id_comanda=%1$s";
    private static final String consultar_todas_refeicoes = "http://rest-dsdois.rhcloud.com/read_refeicao.php";
    private static final String consultar_todas_comandas = "http://rest-dsdois.rhcloud.com/read_comanda.php";
    private static final String remover_pedido = "http://rest-dsdois.rhcloud.com/delete_pedido.php?id_pedido=%1$s";
    private static final String criar_pedido = "http://rest-dsdois.rhcloud.com/create_pedido.php";
    private static final String criar_pedidos = "http://rest-dsdois.rhcloud.com/create_pedidos.php";

    private static final String consultar_vendas = "http://rest-dsdois.rhcloud.com/comanda_por_data.php?data=%1$s";
    private static final String consultar_fornecedor = "http://rest-dsdois.rhcloud.com/read_fornecedor.php";
    private static final String gastos = "http://rest-dsdois.rhcloud.com/gasto_comanda_data.php?data=%1$s";
    private static final String ganhos = "http://rest-dsdois.rhcloud.com/total_comanda_data.php?data=%1$s";
    private static final String pagar_comanda = "http://rest-dsdois.rhcloud.com/fechar_comanda.php?id_comanda=%1$s&valor_total=%2$s";
    private static final String alterar_cliente_mesa_comanda = "http://rest-dsdois.rhcloud.com/update_comanda.php?cliente=%1$s&mesa=%2$s&id_comanda=%3$s";

    public static String getFuncionarios(){
        return consultar_funcionarios;
    }

    public static String getConsultar_Comandas(){
        return consultar_todas_comandas;
    }

    public static String getRemover_comanda(String id_comanda) {
        return String.format(remover_comanda,id_comanda);
    }
    public static String getCriar_comanda(String cliente, String garcom_cpf, String mesa, String data_hora) {
        return String.format(criar_comanda,cliente,garcom_cpf,mesa,data_hora);
    }

    public static String getConsultarVendas(String data) {
        return String.format(consultar_vendas, data);
    }

    public static String getGastos(String data) {
        return String.format(gastos, data);
    }

    public static String getGanhos(String data) {
        return String.format(ganhos, data);
    }

    public static String getConsultarFornecedor() {
        return consultar_fornecedor;
    }

    public static String getConsultar_todas_refeicoes() {
        return consultar_todas_refeicoes;
    }
    public static String getRemover_pedido(String id) {
        return String.format(remover_pedido,id);
    }
    public static String getCriar_pedido() {
        return criar_pedido;
    }
    public static String getCriar_pedidos() {
        return criar_pedidos;
    }
    public static String getPagar_comanda(String id_comanda, String valor_total){
        return String.format(pagar_comanda,id_comanda, valor_total);}


    public static String getAlterarClienteMesaComanda(String cliente, String mesa, String comanda) {
        return String.format(alterar_cliente_mesa_comanda,cliente,mesa, comanda);
    }
}
