package br.ufs.ds2.restaurante.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.adapters.Pedidos_ComandaAdapter;
import br.ufs.ds2.restaurante.controllers.AppController;
import br.ufs.ds2.restaurante.interfaces.ExitDialog;
import br.ufs.ds2.restaurante.models.Comanda;
import br.ufs.ds2.restaurante.models.Garçom;
import br.ufs.ds2.restaurante.models.Pedido;
import br.ufs.ds2.restaurante.settings.RestauranteSettings;
import br.ufs.ds2.restaurante.util.DividerItemDecoration;

public class AbrirComandaActivity extends AppCompatActivity implements ExitDialog{

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private Pedidos_ComandaAdapter adapter;
    private ArrayList<Pedido> pedidos;
    private int id_comanda;
    private TextView valor;
    private Float valor_total;

    public ArrayList<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(Pedido pedido) {
        this.pedidos.add(pedido);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abrir_comanda);

        // ActionBar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Spinner
        final Spinner spinner = (Spinner) findViewById(R.id.spinner_num_mesa);
        Integer[] spinnerArray = {1,2,3,4,5,6,7,8,9}; // ALTERAR
        ArrayAdapter<Integer> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);

        // RecyclerView
        recyclerView = (RecyclerView) findViewById(R.id.lista_pedidos);
        // LayoutManager
        LinearLayoutManager llmanager = new LinearLayoutManager(getApplicationContext());

        // Adapter
        pedidos = new ArrayList<>();
        adapter = new Pedidos_ComandaAdapter(pedidos, getSupportFragmentManager());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(llmanager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL_LIST));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        // Inicializa valor_total
        valor_total = Float.valueOf(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_abrir_comanda, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_add_pedido: // Add pedidos a comanda
                Intent i = new Intent(this,AddPedidosActivity.class);
                i.putExtra("id",id_comanda);
                startActivityForResult(i, 1);
                break;
            case R.id.action_ok: // Abre Comanda
                EditText nome = (EditText) findViewById(R.id.nome_cliente);
                Spinner spinner_num_mesa = (Spinner) findViewById(R.id.spinner_num_mesa);
                int num_mesa = Integer.parseInt(spinner_num_mesa.getSelectedItem().toString());
                Comanda novaComanda = null;
                int valorTotal = 0;
                for(Pedido pedido : pedidos){
                    valorTotal += pedido.getRefeicao().getPreço();
                }

                // Adiciona cliente à comanda
                String cliente = nome.getText().toString();
                novaComanda = new Comanda(id_comanda,cliente,pedidos,new Garçom("","","","",Float.valueOf(0)),valorTotal,num_mesa);

                // Cria comanda no webservice
                cadastrarComanda(novaComanda);

                Intent in = new Intent();
                in.putExtra("Comanda",novaComanda);
                setResult(-1,in);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private String getDataFormatada(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);

    }

    private void cadastrarComanda(final Comanda comanda){
        final ProgressDialog progressDialog = new ProgressDialog(AbrirComandaActivity.this);
        progressDialog.setMessage("Cadastrando comanda...");
        progressDialog.show();
        SharedPreferences settings = getSharedPreferences("garcom", 0);
        String cpf_garcom = settings.getString("cpf","0");
        StringRequest request = new StringRequest(Request.Method.GET,
                RestauranteSettings.getCriar_comanda(comanda.getCliente(), cpf_garcom, // ALTERAR 3333333
                        String.valueOf(comanda.getMesa()), getDataFormatada(comanda.getData_hora()))
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (comanda.getPedidos().size() > 0)
                        cadastrarPedidos(progressDialog,comanda, response.split("/")[0]);
                    else
                        Toast.makeText(getApplicationContext(), "Comanda gerada com sucesso", Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    Log.e("Erro","Erro submetendo comanda ao Webservice");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
            }
        });
        // Adicionando requisição a fila de requisições
        AppController.getInstance().addToRequestQueue(request);
    }

    private void cadastrarPedidos(final ProgressDialog progressDialog, Comanda comanda, String id) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (Pedido pedido: comanda.getPedidos()) {
            JSONObject jobj = new JSONObject();
            jobj.put("id_comanda",id);
            jobj.put("id_refeicao",String.valueOf(pedido.getRefeicao().getId()));
            jsonArray.put(jobj);
        }
        JSONObject jobj = new JSONObject();
        jobj.put("pedidos", jsonArray);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                RestauranteSettings.getCriar_pedidos(),jobj.toString()
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getApplicationContext(), "Comanda gerada com sucesso", Toast.LENGTH_LONG).show();
                progressDialog.hide();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
            }
        });
        // Adicionando requisição a fila de requisições
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            if(resultCode == RESULT_OK) {
                Parcelable[] pedidos = data.getParcelableArrayExtra("Pedidos");
                for (Parcelable pedido : pedidos) {
                    setPedidos((Pedido) pedido);
                    valor_total += ((Pedido) pedido).getRefeicao().getPreço();
                }
                valor = (TextView) findViewById(R.id.valor_total_comanda);
                valor.setText(NumberFormat.getCurrencyInstance().format(valor_total));
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onFinishEditDialog(String id) {
        valor_total -= pedidos.get(Integer.parseInt(id)).getRefeicao().getPreço(); // Altera valorTotal atual
        valor.setText(NumberFormat.getCurrencyInstance().format(valor_total)); // Atualiza UI
        pedidos.remove(Integer.parseInt(id)); // Remove pedido da lista de pedidos
        Toast.makeText(getApplicationContext(), "Pedido removido com sucesso", Toast.LENGTH_LONG).show();
        adapter.notifyDataSetChanged();
    }
}