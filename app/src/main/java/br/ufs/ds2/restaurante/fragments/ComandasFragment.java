package br.ufs.ds2.restaurante.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.melnykov.fab.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.activities.AbrirComandaActivity;
import br.ufs.ds2.restaurante.adapters.ComandasAdapter;
import br.ufs.ds2.restaurante.controllers.AppController;
import br.ufs.ds2.restaurante.interfaces.ExitDialog;
import br.ufs.ds2.restaurante.models.Comanda;
import br.ufs.ds2.restaurante.models.Garçom;
import br.ufs.ds2.restaurante.models.Pedido;
import br.ufs.ds2.restaurante.models.Refeicao;
import br.ufs.ds2.restaurante.settings.RestauranteSettings;

/**
 * Created by Junior on 10/11/2015.
 */
public class ComandasFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private ComandasAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Comanda> comandas;

    // SwypeView
    private SwipeRefreshLayout swipeRefreshLayout;

    public ArrayList<Comanda> getComandas() {
        return comandas;
    }

    public void adicionarComanda(Comanda comanda) {
        this.comandas.add(comanda);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        comandas = new ArrayList<>();
        adapter = new ComandasAdapter(comandas, getFragmentManager(), getContext(), this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_comandas, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        carregarComandas();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        fab.attachToRecyclerView(recyclerView);
        // SwipeViewRefresh Listener
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AbrirComandaActivity.class);
                startActivityForResult(i, 1);

            }
        });
        return view;
    }

    @Override
    public void onRefresh() {
        carregarComandas();
        swipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Remove comanda do sistema
     * @param id_comanda
     */
    private void removerComanda(String id_comanda) {
        StringRequest request = new StringRequest(Request.Method.GET,
                RestauranteSettings.getRemover_comanda(id_comanda),
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getContext(),"Comanda removida com sucesso",Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),"Ocorreu um erro. Tente novamente",Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            if(resultCode == Activity.RESULT_OK) {
                carregarComandas();
            }
        }else if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                removerComanda(data.getStringExtra("pk"));
                carregarComandas();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        carregarComandas();
    }

    /**
     * Carrega comandas do webservice
     */
    private void carregarComandas() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Carregando comandas...");
        progressDialog.show();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                RestauranteSettings.getConsultar_Comandas(),
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                comandas.clear();
                try {
                    JSONArray jsonArray = response.getJSONArray("comandas");
                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject json = (JSONObject) jsonArray.get(i);
                        ArrayList<Pedido> pedidos = null;
                        if (json.getInt("ativa") == 1){
                            if (json.has("pedidos")) {
                                JSONArray pedidosJson = json.getJSONArray("pedidos");
                                pedidos = new ArrayList<>();
                                for (int j = 0; j < pedidosJson.length(); j++) {
                                    JSONObject jobj = (JSONObject) pedidosJson.get(j);
                                    Refeicao refeicao = new Refeicao(Integer.parseInt(jobj.getString("id_refeicao")),
                                            jobj.getString("nome"), jobj.getString("descricao"), Float.parseFloat(jobj.getString("preco")));
                                    pedidos.add(new Pedido(Integer.parseInt(jobj.getString("id_pedido")), refeicao));
                                }
                            }
                            JSONObject garcomJson = json.getJSONObject("garcom");
                            Garçom garcom = new Garçom(garcomJson.getString("nome"),garcomJson.getString("cpf"),
                                    garcomJson.getString("endereco"),garcomJson.getString("telefone"),(float) garcomJson.getDouble("salario"));
                            Comanda comanda = new Comanda(json.getInt("id_comanda"),json.getString("cliente"),
                                    pedidos,garcom,(float) json.getDouble("valorTotal"),json.getInt("mesa"));
                            comandas.add(comanda);
                        }
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    Toast.makeText(getContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
                    progressDialog.hide();
                }
                progressDialog.hide();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
                progressDialog.hide();
            }
        });
        // Adicionando requisição a fila de requisições
        AppController.getInstance().addToRequestQueue(request);
    }
}
