package br.ufs.ds2.restaurante.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.adapters.AdapterFuncionario;
import br.ufs.ds2.restaurante.controllers.AppController;
import br.ufs.ds2.restaurante.models.Funcionario;

public class FuncionarioActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private Toolbar toolbar_principal;
    private RecyclerView recyclerView_funcionarios;
    private RecyclerView.Adapter adapterFuncionarios;
    private RecyclerView.LayoutManager layoutManager_funcionarios;
    static String URL_LIST_GARCOM = "http://rest-dsdois.rhcloud.com/read_garcom.php";
    static String URL_LIST_GERENTE = "http://rest-dsdois.rhcloud.com/read_gerente.php";
    static String URL_DELETE_GARCOM = "http://rest-dsdois.rhcloud.com/delete_garcom.php?cpf=";
    static String URL_DELETE_GERENTE = "http://rest-dsdois.rhcloud.com/delete_gerente.php?cpf=";
    private Drawer.Result navigationDrawerLeft;

    //Dados
    private List<Funcionario> lista_funcionarios;
    // SwypeView
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar_principal = (Toolbar) findViewById(R.id.toolbar_principal);
        toolbar_principal.setTitle("DS2MARKET");
        toolbar_principal.setSubtitle("Bem-vindo");
        setSupportActionBar(toolbar_principal);

        lista_funcionarios = new ArrayList<Funcionario>();
        recyclerView_funcionarios = (RecyclerView) findViewById(R.id.recycler_view_funcionarios);
        recyclerView_funcionarios.setHasFixedSize(true);
        layoutManager_funcionarios = new LinearLayoutManager(this);
        recyclerView_funcionarios.setLayoutManager(layoutManager_funcionarios);
        adapterFuncionarios = new AdapterFuncionario(lista_funcionarios, getApplicationContext());
        getFuncionarios(URL_LIST_GARCOM);
        getFuncionarios(URL_LIST_GERENTE);
        recyclerView_funcionarios.setAdapter(adapterFuncionarios);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.sp_funcionario);
        swipeRefreshLayout.setOnRefreshListener(this);
        //navigation drawer
        navigationDrawerLeft = new Drawer()
                .withActivity(this)
                        //.withToolbar(mToolbar)
                .addDrawerItems(
                        new SecondaryDrawerItem().withName("Cadastrar Funcionário").withIcon(getResources().getDrawable(R.drawable.account_circle)),
                        new SecondaryDrawerItem().withName("Listar Funcionários").withIcon(getResources().getDrawable(R.drawable.account_circle)),
                        new SecondaryDrawerItem().withName("Cadastrar Refeição").withIcon(getResources().getDrawable(R.drawable.account_circle)),
                        new SecondaryDrawerItem().withName("Listar Refeições").withIcon(getResources().getDrawable(R.drawable.account_circle)),
                        new SecondaryDrawerItem().withName("Cadastrar Fornecedor").withIcon(getResources().getDrawable(R.drawable.account_circle)),
                        new SecondaryDrawerItem().withName("Listar Fornecedores").withIcon(getResources().getDrawable(R.drawable.account_circle))

                )
                .withDisplayBelowToolbar(true)
                .withActionBarDrawerToggleAnimated(true)
                .withDrawerGravity(Gravity.LEFT)
                .withSavedInstance(savedInstanceState)
                .withSelectedItem(-1)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                        //Toast.makeText(FuncionarioActivity.this, "onItemClick: " + i, Toast.LENGTH_SHORT).show();
                        Intent nova;
                        switch (i) {
                            case 0:
                                nova = new Intent(getApplicationContext(),CadastrarFuncionarioActivity.class);
                                startActivity(nova);
                                break;
                            case 1:
                                nova = new Intent(getApplicationContext(),FuncionarioActivity.class);
                                startActivity(nova);
                                break;
                            case 2:
                                nova = new Intent(getApplicationContext(),CadastrarRefeicao.class);
                                startActivity(nova);
                                break;
                            case 3:
                                nova = new Intent(getApplicationContext(),RefeicaoActivity.class);
                                startActivity(nova);
                                break;
                            case 4:
                                nova = new Intent(getApplicationContext(), CadastrarFornecedor.class);
                                startActivity(nova);
                                break;
                            case 5:
                                nova = new Intent(getApplicationContext(),FornecedorActivity.class);
                                startActivity(nova);
                                break;
                        }
                    }
                })
                .withOnDrawerItemLongClickListener(new Drawer.OnDrawerItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                        //Toast.makeText(FuncionarioActivity.this, "onItemLongClick: " + i, Toast.LENGTH_SHORT).show();
                        return false;
                    }
                })
                .build();
    }

    @Override
    public void onRefresh() {
        lista_funcionarios.clear();
        adapterFuncionarios.notifyDataSetChanged();
        getFuncionarios(URL_LIST_GARCOM);
        getFuncionarios(URL_LIST_GERENTE);
        swipeRefreshLayout.setRefreshing(false);
    }

    private void loadFuncionarios(JSONObject response, String cargo){
        String tipo="garcons";
        if (cargo.equals("gerente")) {tipo = "gerentes";}
        try {
            JSONArray perguntas = response.getJSONArray(tipo);
            for(int i=0; i < perguntas.length(); i++){
                JSONObject pg = perguntas.getJSONObject(i);
                Funcionario funcionario = new Funcionario(pg.getString("nome"),pg.getString("cpf"),pg.getString("endereco"),
                        pg.getString("telefone"),pg.getString("salario"),pg.getString("dataDeAdmissao"));
                funcionario.setCargo(cargo);
                lista_funcionarios.add(funcionario);
                adapterFuncionarios.notifyItemInserted(lista_funcionarios.size() - 1);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getFuncionarios(final String URL){
        Log.e("getFuncionarios ", URL);
        JsonObjectRequest newsReq = new JsonObjectRequest(Request.Method.GET, URL,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("getFuncionarios ", response.toString());
                        if(URL.contains("garcom")){
                            loadFuncionarios(response,"garcom");
                        }else{
                            loadFuncionarios(response,"gerente");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("getFuncionarios ", "Erro: " + error.getMessage());
            }
        }) {
            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(newsReq);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void excluirFuncionario(String cpf, String tipo, final int position){
        String URL = URL_DELETE_GARCOM;
        if(tipo.equals("gerente")){URL = URL_DELETE_GERENTE;}
        Log.e("excluirFuncionario","URL: "+URL+cpf);
        StringRequest req = new StringRequest(Request.Method.GET, URL+cpf,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response);
                        if (response.contains("ok")){
                            Toast t = Toast.makeText(getApplicationContext(),"FUNCIONÁRO EXCLUÍDO COM SUCESSO",Toast.LENGTH_SHORT);
                            t.show();
                            lista_funcionarios.remove(position);
                            adapterFuncionarios.notifyItemRemoved(position);
                            for(int i=position; i < lista_funcionarios.size(); i++){
                                adapterFuncionarios.notifyItemChanged(i);
                            }
                        }else{
                            Toast t = Toast.makeText(getApplicationContext(),"ESSE FUNCIONÁRIO NÃO PODE SER EXCLUÍDO NO MOMENTO!",Toast.LENGTH_SHORT);
                            t.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("HttpClient", "error: " + error.toString());
                        Toast t = Toast.makeText(getApplicationContext(),"ERRO, TENTE OUTRA VEZ.",Toast.LENGTH_LONG);
                        t.show();
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req);
    }

    public boolean onContextItemSelected(MenuItem item) {
        int position = item.getOrder();
        switch (item.getItemId()) {
            case R.id.call:
                //Toast.makeText(getApplicationContext(),"ID: " + String.valueOf(item.getOrder()) ,Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(),alterar_funcionario_activity.class);
                intent.putExtra("nome",lista_funcionarios.get(position).getNome());
                intent.putExtra("cpf",lista_funcionarios.get(position).getCpf());
                intent.putExtra("endereco",lista_funcionarios.get(position).getEndereco());
                intent.putExtra("telefone",lista_funcionarios.get(position).getTelefone());
                intent.putExtra("salario",lista_funcionarios.get(position).getSalario());
                intent.putExtra("DataAdmissao",lista_funcionarios.get(position).getDataAdmissao());
                intent.putExtra("DataDemissao",lista_funcionarios.get(position).getDataDemissa());
                intent.putExtra("cargo", lista_funcionarios.get(position).getCargo());
                startActivity(intent);
                break;
            case R.id.msg:
                excluirFuncionario(lista_funcionarios.get(position).getCpf(),lista_funcionarios.get(position).getCargo(),position);
                break;
        }
        return super.onContextItemSelected(item);
    }
}
