package br.ufs.ds2.restaurante.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.List;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.models.Refeicao;

/**
 * Created by Breno Cruz on 28/11/2015.
 */
public class CardapioAdapter extends RecyclerView.Adapter<CardapioAdapter.MyViewHolder>{

    private List<Refeicao> mList;
    private LayoutInflater mLayoutInflater;


    public CardapioAdapter(Context c, List<Refeicao> r){
        mList = r;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.adapter_cardapio, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        NumberFormat format = NumberFormat.getCurrencyInstance();

        holder.nomeRefeicao.setText(mList.get(position).getNome());
        holder.descricaoRefeicao.setText(mList.get(position).getDescricao());
        holder.valorRefeicao.setText(format.format(mList.get(position).getPreço()));
        holder.valorDesconto.setText("Desconto: " + format.format(mList.get(position).getPreço() * mList.get(position).getDesconto()));

        holder.valorQuantidade.setText("Quantidade: " + mList.get(position).getQuantidade());
        if(mList.get(position).getQuantidade() > 0){
            holder.situacaoQuantidade.setText("Disponível");
            holder.situacaoQuantidade.setTextColor(Color.parseColor("#00b300"));
        }else{
            holder.situacaoQuantidade.setText("Indisponível");
            holder.situacaoQuantidade.setTextColor(Color.parseColor("#800000"));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView nomeRefeicao;
        public TextView descricaoRefeicao;
        public TextView valorRefeicao;
        public TextView valorDesconto;
        public TextView valorQuantidade;
        public TextView situacaoQuantidade;

        public MyViewHolder(View itemView) {
            super(itemView);

            nomeRefeicao = (TextView) itemView.findViewById(R.id.textNome);
            descricaoRefeicao = (TextView) itemView.findViewById(R.id.textDescricao);
            valorRefeicao = (TextView) itemView.findViewById(R.id.textPreco);
            valorDesconto = (TextView) itemView.findViewById(R.id.textDesconto);
            valorQuantidade = (TextView) itemView.findViewById(R.id.textQuantidade);
            situacaoQuantidade = (TextView) itemView.findViewById(R.id.textSituacaoQtd);

        }
    }
}
