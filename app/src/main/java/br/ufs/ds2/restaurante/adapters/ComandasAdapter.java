package br.ufs.ds2.restaurante.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.activities.VerComandaActivity;
import br.ufs.ds2.restaurante.dialogs.RemoverComandaDialog;
import br.ufs.ds2.restaurante.fragments.ComandasFragment;
import br.ufs.ds2.restaurante.models.Comanda;

/**
 * Created by Junior on 11/11/2015.
 */
public class ComandasAdapter extends RecyclerView.Adapter<ComandasAdapter.ViewHolder> {

    private List<Comanda> comandasList;
    private static FragmentManager fragManager;
    public Context context;
    private ComandasFragment fragment;

    public ComandasAdapter(List<Comanda> comments, FragmentManager fragManager, Context context, ComandasFragment fragment) {
        this.comandasList = comments;
        ComandasAdapter.fragManager = fragManager;
        this.context = context;
        this.fragment = fragment;
    }

    // Create new view (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context)
                .inflate(R.layout.item_comanda, parent, false);
        // Return a new holder instance
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    // - get element from dataset
    // - replace the contents of the view with that element
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Comanda comanda = comandasList.get(position);
        // Set comment attributes
        holder.numero_comanda = comanda.getId();
        holder.vComanda.setText(Integer.toString(comanda.getId()));
        holder.vMesa.setText(Integer.toString(comanda.getMesa()));
        holder.vPedidos.setText(Integer.toString(comanda.getNumPedidos()));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return comandasList.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {

        private int numero_comanda;
        private TextView vComanda;
        private TextView vMesa;
        private TextView vPedidos;

        public ViewHolder(View view) {
            super(view);
            this.vComanda = (TextView) view.findViewById(R.id.num_comanda);
            this.vMesa = (TextView) view.findViewById(R.id.data_v);
            this.vPedidos = (TextView) view.findViewById(R.id.num_pedidos);
            // Attach a onLongClickListener to the entire row view for exclude comment Dialog
            view.setOnLongClickListener(this);
            view.setOnClickListener(this);
        }

        // Shows exclude comment dialog
        @Override
        public boolean onLongClick(View v) {
            DialogFragment newFragment = RemoverComandaDialog.newInstance(numero_comanda);
            FragmentTransaction ft = fragManager.beginTransaction();
            ft.commit();
            newFragment.setTargetFragment(fragment, 1);
            newFragment.show(fragManager, "dialog");
            return true;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, VerComandaActivity.class);
            intent.putExtra("Comanda",comandasList.get(getLayoutPosition()));
            context.startActivity(intent);
        }
    }
}
