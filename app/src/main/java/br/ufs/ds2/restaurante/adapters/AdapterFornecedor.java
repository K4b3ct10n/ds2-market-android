package br.ufs.ds2.restaurante.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.models.Fornecedor;

/**
 * Created by Desconhecido on 28/11/2015.
 */
public class AdapterFornecedor extends RecyclerView.Adapter<AdapterFornecedor.FornecedorHolder> {
    private List<Fornecedor> lista_fornecedor;
    private LayoutInflater layoutinflater;
    static Context contexto;
    ContextMenu.ContextMenuInfo info;

    public AdapterFornecedor(List<Fornecedor> list_fornecedor, Context contexto) {
        this.contexto = contexto;
        lista_fornecedor = list_fornecedor;
        layoutinflater = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public AdapterFornecedor(){}

    public static class FornecedorHolder extends RecyclerView.ViewHolder implements  View.OnClickListener, View.OnCreateContextMenuListener {
        TextView tv_forn_nome;
        TextView tv_forn_cnpj;
        TextView tv_forn_endereco;
        TextView tv_forn_telefone;
        int position;

        public FornecedorHolder(View itemView) {
            super(itemView);
            tv_forn_nome = (TextView) itemView.findViewById(R.id.tv_forn_nome);
            tv_forn_cnpj = (TextView) itemView.findViewById(R.id.tv_forn_cnpj);
            tv_forn_endereco = (TextView) itemView.findViewById(R.id.tv_forn_endereco);
            tv_forn_telefone = (TextView) itemView.findViewById(R.id.tv_forn_telefone);
            itemView.setOnClickListener(this);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            new AdapterFornecedor().info = menuInfo;
            menu.setHeaderTitle("Operações");
            menu.add(0, R.id.call, position, "Alterar");//groupId, itemId, order, title
            menu.add(0, R.id.msg, position, "Excluir");

            //TextView tv = (TextView) v.findViewById(R.id.tv_nome);
            //tv.getText()
        }
    }

    @Override
    public AdapterFornecedor.FornecedorHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutinflater.inflate(R.layout.view_fornecedor,parent,false);
        FornecedorHolder vh = new FornecedorHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(FornecedorHolder holder, int position) {
        holder.tv_forn_nome.setText(lista_fornecedor.get(position).getNome());
        holder.tv_forn_endereco.setText(lista_fornecedor.get(position).getEndereco());
        holder.tv_forn_cnpj.setText(lista_fornecedor.get(position).getCnpj());
        holder.tv_forn_telefone.setText(lista_fornecedor.get(position).getTelefone());
        holder.position = position;
    }

    @Override
    public int getItemCount() {
        return lista_fornecedor.size();
    }

}
