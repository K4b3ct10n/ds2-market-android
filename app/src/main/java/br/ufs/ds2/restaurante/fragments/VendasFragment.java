package br.ufs.ds2.restaurante.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.adapters.VendasAdapter;
import br.ufs.ds2.restaurante.controllers.AppController;
import br.ufs.ds2.restaurante.models.Comanda;
import br.ufs.ds2.restaurante.settings.RestauranteSettings;

/**
 * Created by Junior on 10/11/2015.
 */
public class VendasFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private RecyclerView recyclerView;
    private VendasAdapter adapter;
    private String dataInicial;//Data passada como parâmetro da activity DataActivity
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Comanda> comandas; //Lista de comandas
    // SwypeView
    private SwipeRefreshLayout swipeRefreshLayout;

    public VendasFragment(String data) {
        this.dataInicial = data;
    }

    //Acessa a lista
    public ArrayList<Comanda> getComandas() {
        return comandas;
    }
    //Adiciona na lista
    public void adicionarComanda(Comanda comanda) {
        this.comandas.add(comanda);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        comandas = new ArrayList<>();

        //Preenchendo o cardview com a lista de vendas
        preencherLista();
        adapter = new VendasAdapter(comandas, getFragmentManager());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_vendas, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.listVendas);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        // SwipeViewRefresh Listener
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        //dataInicial = getArguments().getString("dataInicial");

        return view;
    }

    //Método responsável por realizar as consultas via JSON e/ou web service
    private void preencherLista(){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, RestauranteSettings.getConsultarVendas(dataInicial)
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray arrayJson = null;
                try {
                    arrayJson = response.getJSONArray("comandas");
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    for (int i=0; i<arrayJson.length();i++) {
                        JSONObject comanda = (JSONObject) arrayJson.get(i);

                        comandas.add(new Comanda(comanda.getInt("id_comanda"), formatter.parse(comanda.getString("data_hora"))
                                , (float) comanda.getDouble("valorTotal")));
                        adapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    Log.e("VendasFragment", e.getMessage());
                } catch (ParseException e) {
                    Log.e("VendasFragment Data", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Consulta vazia para esta data!", Toast.LENGTH_LONG).show();
            }
        });
        // Adicionando requisição a fila de requisições
        AppController.getInstance().addToRequestQueue(request);

    }


    //Métodos auxiliares
    @Override
    public void onRefresh() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        adapter.notifyDataSetChanged();

    }
}
