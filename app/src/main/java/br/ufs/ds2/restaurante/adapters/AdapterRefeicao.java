package br.ufs.ds2.restaurante.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.models.Refeicao;

/**
 * Created by Desconhecido on 11/11/2015.
 */
public class AdapterRefeicao extends RecyclerView.Adapter<AdapterRefeicao.RefeicaoHolder> {
    private List<Refeicao> lista_refeicao;
    private LayoutInflater layoutinflater;
    static Context contexto;
    ContextMenu.ContextMenuInfo info;

    public AdapterRefeicao(List<Refeicao> list_refeicao, Context contexto) {
        this.contexto = contexto;
        lista_refeicao = list_refeicao;
        layoutinflater = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public AdapterRefeicao(){}

    public static class RefeicaoHolder extends RecyclerView.ViewHolder implements  View.OnClickListener, View.OnCreateContextMenuListener {
        TextView tv_nome;
        TextView tv_preco;
        int position;

        public RefeicaoHolder(View itemView) {
            super(itemView);
            tv_nome = (TextView) itemView.findViewById(R.id.tv_nome);
            tv_preco = (TextView) itemView.findViewById(R.id.tv_preco);
            itemView.setOnClickListener(this);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View v) {}

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            new AdapterRefeicao().info = menuInfo;
            menu.setHeaderTitle("Operações");
            menu.add(0, R.id.call, position, "Alterar");//groupId, itemId, order, title
            menu.add(0, R.id.msg, position, "Excluir");
        }
    }

    @Override
    public AdapterRefeicao.RefeicaoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutinflater.inflate(R.layout.view_refeicao,parent,false);
        RefeicaoHolder vh = new RefeicaoHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RefeicaoHolder holder, int position) {
            holder.tv_nome.setText(lista_refeicao.get(position).getNome());
            holder.tv_preco.setText(String.valueOf(lista_refeicao.get(position).getPreço()));
            holder.position = position;
    }

    @Override
    public int getItemCount() {
        return lista_refeicao.size();
    }
}
