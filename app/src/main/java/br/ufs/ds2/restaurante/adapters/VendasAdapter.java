package br.ufs.ds2.restaurante.adapters;


import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.dialogs.RemoverComandaDialog;
import br.ufs.ds2.restaurante.models.Comanda;

/**
 * Created by Junior on 11/11/2015.
 */
public class VendasAdapter extends RecyclerView.Adapter<VendasAdapter.ViewHolder> {

    /*
   * Classe responsável por fazer referências aos dados comandas em um período e alimentar os fragmentos de consultas
   * */
    private List<Comanda> comandasList;
    private static FragmentManager fragManager;

    public VendasAdapter(List<Comanda> comments, FragmentManager fragManager) {
        this.comandasList = comments;
        VendasAdapter.fragManager = fragManager;
    }

    // Create new view (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_vendas, parent, false);
        // Return a new holder instance
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    // - get element from dataset
    // - replace the contents of the view with that element
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Comanda comanda = comandasList.get(position);
        // Set comment attributes
        holder.numero_comanda = comanda.getId();
        holder.vComanda.setText(Integer.toString(comanda.getId()));
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        holder.vData.setText(df.format(comanda.getData_hora()));
        holder.vValor.setText(Float.toString(comanda.getValorTotal()));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return comandasList.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        //Atributos das comandas da activity
        private int numero_comanda;
        private TextView vComanda;
        private TextView vData;
        private TextView vValor;

        public ViewHolder(View view) {
            super(view);
            this.vComanda = (TextView) view.findViewById(R.id.num_comanda);
            this.vData = (TextView) view.findViewById(R.id.endereco);
            this.vValor = (TextView) view.findViewById(R.id.valorTotal_V);
            // Attach a onLongClickListener to the entire row view for exclude comment Dialog
            view.setOnLongClickListener(this);
        }

        // Shows exclude comment dialog
        @Override
        public boolean onLongClick(View v) {

            return true;
        }
    }
}
