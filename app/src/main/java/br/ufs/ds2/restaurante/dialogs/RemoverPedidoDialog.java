package br.ufs.ds2.restaurante.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.interfaces.ExitDialog;

/**
 * Created by Junior on 27/08/2015.
 */
public class RemoverPedidoDialog extends DialogFragment {

    private static final String TAG = "RemoverPedidoDialog";
    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static RemoverPedidoDialog newInstance(int pk) {
        RemoverPedidoDialog dialog = new RemoverPedidoDialog();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("pk", Integer.toString(pk));
        dialog.setArguments(args);
        return dialog;
    }

    public RemoverPedidoDialog(){
        setArguments(getArguments());
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int pk = getArguments().getInt("pk");
        // Use the Builder class for convenient dialog construction
        return new AlertDialog.Builder(getActivity())
                .setMessage("Deseja remover este pedido?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ExitDialog activity = (ExitDialog) getActivity();
                        activity.onFinishEditDialog(getArguments().getString("pk"));
                        dismiss();
                        /*Fragment targetFragment = getTargetFragment();
                        Intent intent = new Intent();
                        intent.putExtra("pk",getArguments().getString("pk"));
                        if (targetFragment != null) {
                            targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                        }*/
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                })
                .create();
    }
}
