package br.ufs.ds2.restaurante.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.adapters.CardapioAdapter;
import br.ufs.ds2.restaurante.adapters.PedidosAdapter;
import br.ufs.ds2.restaurante.controllers.AppController;
import br.ufs.ds2.restaurante.models.Refeicao;
import br.ufs.ds2.restaurante.settings.RestauranteSettings;

/**
 * Created by Junior on 11/11/2015.
 */
public class CardapioFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private CardapioAdapter mCardapioAdapter;
    private ArrayList<Refeicao> mList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pedidos, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_list);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);

        carregarRefeicoes();
        mCardapioAdapter = new CardapioAdapter(getActivity(), mList);
        //mPedidosAdapter.setRecycleViewOnCLickListenerHack(this);
        mRecyclerView.setAdapter(mCardapioAdapter);

        return view;
    }

    private void carregarRefeicoes() {
        mList = new ArrayList<>();
        StringRequest request = new StringRequest(Request.Method.GET, RestauranteSettings.getConsultar_todas_refeicoes()
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jobj = new JSONObject(response).getJSONArray("refeicoes");
                    for(int i = 0; i < jobj.length(); i++){
                        JSONObject jsonObject = (JSONObject) jobj.get(i);
                        Refeicao refeicao = new Refeicao(jsonObject.getInt("id_refeicao"),jsonObject.getString("nome"),jsonObject.getString("descricao"),
                                (float) jsonObject.getDouble("preco"),(float) jsonObject.getDouble("desconto"), jsonObject.getInt("quantidade"));
                        mList.add(refeicao);
                        mCardapioAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
            }
        });
        // Adicionando requisição a fila de requisições
        AppController.getInstance().addToRequestQueue(request);
    }
}
