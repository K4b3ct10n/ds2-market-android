package br.ufs.ds2.restaurante.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Junior on 10/11/2015.
 * Constructor 1: Comanda(int id, Cliente cliente, List<Pedido> pedidos, Garçom garcom, int mesa, int tipoPagamento, float valorTotal, Date data_hora, boolean ativa)
 * Constructor 2: Comanda(int id, Garçom garcom, int mesa)
 */
public class Comanda implements Parcelable {

    private int id;
    private String cliente;
    private List<Pedido> pedidos;
    private Garçom garcom;
    private int mesa;
    private Boolean ativa;
    private float valorTotal;
    private Date data_hora;

    public Comanda(int id, String cliente, List<Pedido> pedidos, Garçom garcom, float valor_total, int mesa) {
        this.id = id;
        this.cliente = cliente;
        this.pedidos = pedidos;
        this.garcom = garcom;
        this.mesa = mesa;
        this.ativa = true;
        this.valorTotal = valor_total;
        this.data_hora = new Date();
    }

    public Comanda(int id, String cliente, Garçom garcom, float valor_total, int mesa) {
        this.id = id;
        this.cliente = cliente;
        this.garcom = garcom;
        this.mesa = mesa;
        this.ativa = true;
        this.valorTotal = valor_total;
        this.data_hora = new Date();
    }

    public Comanda(int id, Garçom garcom, int mesa){
        this.id = id;
        this.garcom = garcom;
        this.mesa = mesa;
        this.ativa = true;
    }

    //Construtor usado para realização das consultas do web service, passando os parâmetros
    public Comanda(int id, Date data, float valor){
        this.id = id;
        this.data_hora = data;
        this.valorTotal = valor;
    }

    protected Comanda(Parcel in) {
        this.id = in.readInt();
        this.cliente = in.readString();
        this.pedidos = new ArrayList<Pedido>();
        in.readList(this.pedidos,Pedido.class.getClassLoader());
        this.garcom = (Garçom) in.readValue(Garçom.class.getClassLoader());
        this.mesa = in.readInt();
        this.ativa = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.valorTotal = in.readFloat();
    }

    public static final Creator<Comanda> CREATOR = new Creator<Comanda>() {
        @Override
        public Comanda createFromParcel(Parcel in) {
            return new Comanda(in);
        }

        @Override
        public Comanda[] newArray(int size) {
            return new Comanda[size];
        }
    };

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Boolean getAtiva() {
        return ativa;
    }

    public void setAtiva(Boolean ativa) {
        this.ativa = ativa;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public int getNumPedidos(){ if (pedidos != null)  return pedidos.size(); else return 0 ;}

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    public Garçom getGarcom() {
        return garcom;
    }

    public void setGarcom(Garçom garcom) {
        this.garcom = garcom;
    }

    public int getMesa() {
        return mesa;
    }

    public void setMesa(int mesa) {
        this.mesa = mesa;
    }

    public boolean isAtiva() {
        return ativa;
    }

    public void setAtiva(boolean ativa) {
        this.ativa = ativa;
    }

    public float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Date getData_hora() {
        return data_hora;
    }

    public void setData_hora(Date data_hora) {
        this.data_hora = data_hora;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(cliente);
        dest.writeList(pedidos);
        dest.writeValue(garcom);
        dest.writeInt(mesa);
        dest.writeValue(ativa);
        dest.writeFloat(valorTotal);
    }
}
