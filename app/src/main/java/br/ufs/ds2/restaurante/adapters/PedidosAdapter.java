package br.ufs.ds2.restaurante.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.interfaces.RecycleViewOnCLickListenerHack;
import br.ufs.ds2.restaurante.models.Pedido;

/**
 * Created by Breno Cruz on 11/11/2015.
 */
public class PedidosAdapter extends RecyclerView.Adapter<PedidosAdapter.MyViewHolder>{

    private List<Pedido> mList;
    private LayoutInflater mLayoutInflater;
    private RecycleViewOnCLickListenerHack mRecycleViewOnCLickListenerHack;

    public PedidosAdapter(Context c, List<Pedido> l){
        mList = l;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //Cria as views
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.adapter_pedidos, parent, false);
        return new MyViewHolder(view);
    }

    //Chama as views ja criadas
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.numPedido.setText("Pedido número "+mList.get(position).getNumPedido());
        holder.nomeRefeicao.setText(mList.get(position).getRefeicao().getNome());
        holder.numComanda.setText("Comanda "+mList.get(position).getIdComanda());

    }

    public void addItemList(Pedido p, int position){
        mList.add(p);
        notifyItemInserted(position);
    }

    public void removeItemList(int position){
        mList.remove(position);
        notifyItemRemoved(position);
    }

    public void setRecycleViewOnCLickListenerHack(RecycleViewOnCLickListenerHack r){
        mRecycleViewOnCLickListenerHack = r;
    }


    //Tamanho da lista
    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView numPedido;
        public TextView nomeRefeicao;
        public TextView numComanda;

        public MyViewHolder(View itemView) {
            super(itemView);

            numPedido = (TextView) itemView.findViewById(R.id.numPedido);
            nomeRefeicao = (TextView) itemView.findViewById(R.id.nomeRefeicao);
            numComanda = (TextView) itemView.findViewById(R.id.numComanda);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecycleViewOnCLickListenerHack != null){
                mRecycleViewOnCLickListenerHack.onClickListener(v, getLayoutPosition());
            }
        }
    }
}
