package br.ufs.ds2.restaurante.activities;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.OnAccountsUpdateListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.adapters.GarcomViewPagerAdapter;
import br.ufs.ds2.restaurante.fragments.CardapioFragment;
import br.ufs.ds2.restaurante.fragments.ComandasFragment;
import br.ufs.ds2.restaurante.fragments.PedidosFragment;
import br.ufs.ds2.restaurante.interfaces.ExitDialog;

public class GarcomActivity extends AppCompatActivity{

    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private AccountManager accountManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_garcom);

        // Getting gerenciador de contas
        accountManager = AccountManager.get(getApplicationContext());
        // AccountManager listenner for logout
        // Listenner for logout
        OnAccountsUpdateListener accountsListener = new OnAccountsUpdateListener() {
            @Override
            public void onAccountsUpdated(Account[] accounts) {
                Account[] myAccounts = AccountManager.get(getApplicationContext()).getAccountsByType(LoginActivity.ACCOUNT_MANAGER_TYPE);
                if (myAccounts.length == 0) {
                    // The account has been deleted
                    Intent i = new Intent(GarcomActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        };
        accountManager.addOnAccountsUpdatedListener(accountsListener, null, true);

        // ActionBar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Get viewPager and tab layouts
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        // Set up viewPager
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch(item.getItemId()){
            case R.id.action_exit:
                Account[] myAccounts = AccountManager.get(getApplicationContext()).getAccountsByType(LoginActivity.ACCOUNT_MANAGER_TYPE);
                accountManager.removeAccount(myAccounts[0], null, null);
                break;
        }
        return true;
    }

    // Setup viewPager adapter and fragments
    private void setupViewPager(ViewPager viewPager) {
        GarcomViewPagerAdapter adapter = new GarcomViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ComandasFragment(), "Comandas");
        adapter.addFragment(new PedidosFragment(), "Pedidos");
        adapter.addFragment(new CardapioFragment(), "Cardápio");
        viewPager.setAdapter(adapter);
    }
}
