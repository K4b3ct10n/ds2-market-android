package br.ufs.ds2.restaurante.interfaces;

/**
 * Created by Junior on 17/11/2015.
 */
public interface ExitDialog {
    void onFinishEditDialog(String slug);
}
