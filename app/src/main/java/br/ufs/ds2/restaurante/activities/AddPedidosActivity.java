package br.ufs.ds2.restaurante.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.adapters.AddPedidosAdapter;
import br.ufs.ds2.restaurante.controllers.AppController;
import br.ufs.ds2.restaurante.models.Pedido;
import br.ufs.ds2.restaurante.models.Refeicao;
import br.ufs.ds2.restaurante.settings.RestauranteSettings;

public class AddPedidosActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayout;
    private AddPedidosAdapter adapter;
    private ArrayList<Refeicao> refeições;
    private ArrayList<Pedido> pedidos;

    public ArrayList<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(ArrayList<Pedido> pedidos) {
        this.pedidos.addAll(pedidos);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pedidos);

        // ActionBar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // RecyclerView
        recyclerView = (RecyclerView) findViewById(R.id.list);

        pedidos = new ArrayList<>();
        refeições = new ArrayList<>();
        linearLayout = new LinearLayoutManager(getApplicationContext());

        adapter = new AddPedidosAdapter(refeições);
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setAdapter(adapter);

        // Consulta pedidos
        final ProgressDialog progressDialog = new ProgressDialog(AddPedidosActivity.this);
        progressDialog.setMessage("Carregando refeições...");
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.GET,RestauranteSettings.getConsultar_todas_refeicoes()
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jobj = new JSONObject(response).getJSONArray("refeicoes");
                    for(int i = 0; i < jobj.length(); i++){
                        JSONObject jsonObject = (JSONObject) jobj.get(i);
                        Refeicao refeicao = new Refeicao(jsonObject.getInt("id_refeicao"),jsonObject.getString("nome"),jsonObject.getString("descricao"),
                                (float) jsonObject.getDouble("preco"),(float) jsonObject.getDouble("desconto"), jsonObject.getInt("quantidade"));
                        refeições.add(refeicao);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.hide();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
                finish();
            }
        });
        // Adicionando requisição a fila de requisições
        AppController.getInstance().addToRequestQueue(request);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_pedidos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_ok: // Add Pedidos a Comanda
                Intent i = new Intent();
                Pedido[] pedidosArray = new Pedido[adapter.getPedidos().size()];
                adapter.getPedidos().toArray(pedidosArray);
                i.putExtra("Pedidos", pedidosArray);
                setResult(RESULT_OK, i);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
