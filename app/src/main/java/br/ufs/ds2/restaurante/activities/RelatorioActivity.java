package br.ufs.ds2.restaurante.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.adapters.GarcomViewPagerAdapter;
import br.ufs.ds2.restaurante.fragments.FornecedorFragment;
import br.ufs.ds2.restaurante.fragments.GanhosFragment;
import br.ufs.ds2.restaurante.fragments.GastosFragment;
import br.ufs.ds2.restaurante.fragments.VendasFragment;

public class RelatorioActivity extends AppCompatActivity {
    /*
    * Classe responsável por conectar of fragmentos das consultas
    * */

    private Toolbar toolbar;

    private ViewPager viewPager;
    private TabLayout tabLayout;

    //Data que recebe o parâmetro passado por DataActivity
    private String dataInicial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorios);

        // ActionBar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Get viewPager and tab layouts
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        // Pega o atributo passado pela intent anterior (DataActivity)
        Intent intent = getIntent();
        dataInicial = intent.getStringExtra(DataActivity.EXTRA_MESSAGE);

        // Set up viewPager
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    public RelatorioActivity(String data) {
        this.dataInicial = data;
    }
    public RelatorioActivity(){}
    // Setup viewPager adapter and fragments

    //Método responsável por fazer a refência aos fragmentos
    private void setupViewPager(ViewPager viewPager) {
        GarcomViewPagerAdapter adapter = new GarcomViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new VendasFragment(dataInicial), "Vendas");
        adapter.addFragment(new GastosFragment(dataInicial), "Gastos");
        adapter.addFragment(new GanhosFragment(dataInicial), "Ganho");
        adapter.addFragment(new FornecedorFragment(), "Fornecedor");
        viewPager.setAdapter(adapter);
    }
}
