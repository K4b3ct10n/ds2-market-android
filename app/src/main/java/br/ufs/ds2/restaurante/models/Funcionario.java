package br.ufs.ds2.restaurante.models;

import java.util.Date;

/**
 * Created by Desconhecido on 10/11/2015.
 */
public class Funcionario {
    private int order;
    private String nome;
    private String cpf;
    private String endereco;
    private String telefone;
    private double salario;
    private String dataAdmissao;
    private String dataDemissa;
    private String cargo;
    private String tipoFuncionario;

    public Funcionario(String nome){
        this.nome = nome;
    }

    public Funcionario(String nome, String cpf, String endereco, String telefone, String salario, String dataDeAdmissao){
        this.nome = nome;
        this.endereco = endereco;
        this.cpf = cpf;
        this.salario = Double.valueOf(salario);
        this.dataAdmissao = dataDeAdmissao;
        this.telefone = telefone;
    }

    public Funcionario(String nome, String cargo){
        this.nome = nome;
        this.cargo = cargo;
    }

    public int getOrder(){return order;}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(String dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public String getDataDemissa() {
        return dataDemissa;
    }

    public void setDataDemissa(String dataDemissa) {
        this.dataDemissa = dataDemissa;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getTipoFuncionario() {
        return tipoFuncionario;
    }

    public void setTipoFuncionario(String tipoFuncionario) {
        this.tipoFuncionario = tipoFuncionario;
    }

    public void registrarHoraExtra(int qtdHora){
    }

    public void registraDemissa(Date data){
    }

    public boolean cadastrar(){
        return true;
    }

    public boolean excluir(){
        return true;
    }
}
