package br.ufs.ds2.restaurante.adapters;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.dialogs.RemoverPedidoDialog;
import br.ufs.ds2.restaurante.dialogs.RemoverPedido_VerPedido_Dialog;
import br.ufs.ds2.restaurante.models.Pedido;

/**
 * Created by Junior on 11/11/2015.
 */
public class Ver_Pedidos_ComandaAdapter extends RecyclerView.Adapter<Ver_Pedidos_ComandaAdapter.ViewHolder> {

    private ArrayList<Pedido> pedidos;
    private static FragmentManager fragManager;

    public Ver_Pedidos_ComandaAdapter(ArrayList<Pedido> pedidos, FragmentManager fragManager) {
        this.pedidos = pedidos;
        Ver_Pedidos_ComandaAdapter.fragManager = fragManager;
    }
    // Create new view (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context)
                .inflate(R.layout.item_pedidos_abrir_comanda, parent, false);
        // Return a new holder instance
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    // - get element from dataset
    // - replace the contents of the view with that element
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Pedido pedido = pedidos.get(position);

        holder.id = pedido.getId();
        holder.position = position;
        holder.vRefeicao.setText(pedido.getRefeicao().getNome());
        holder.vOrdem.setText(Integer.toString(position+1));
        NumberFormat format = NumberFormat.getCurrencyInstance();
        holder.vValor.setText(format.format(pedido.getRefeicao().getPreço()));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return pedidos.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        private int id;
        private int position;
        private TextView vRefeicao;
        private TextView vOrdem;
        private TextView vValor;

        public ViewHolder(View view) {
            super(view);
            this.vRefeicao = (TextView) view.findViewById(R.id.nome_refeicao);
            this.vOrdem = (TextView) view.findViewById(R.id.ordem_do_pedido);
            this.vValor = (TextView) view.findViewById(R.id.valor);
            // Attach a onLongClickListener to the entire row view for exclude comment Dialog
            view.setOnLongClickListener(this);
        }

        // Shows exclude comment dialog
        @Override
        public boolean onLongClick(View v) {
            DialogFragment newFragment = RemoverPedido_VerPedido_Dialog.newInstance(id, position);

            FragmentTransaction ft = fragManager.beginTransaction();
            ft.commit();
            newFragment.setTargetFragment(newFragment, 2);
            newFragment.show(fragManager, "dialog");
            return true;
        }
    }
}
