package br.ufs.ds2.restaurante.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.models.Pedido;
import br.ufs.ds2.restaurante.models.Refeicao;

/**
 * Created by Junior on 11/11/2015.
 */
public class AddPedidosAdapter extends RecyclerView.Adapter<AddPedidosAdapter.ViewHolder> {

    private static final String TAG = "AddPedidosAdapter";
    private List<Refeicao> refeicoes;
    private List<Pedido> pedidos;

    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public AddPedidosAdapter(List<Refeicao> refeicoes) {
        this.refeicoes = refeicoes;
        this.pedidos = new ArrayList<>();
    }

    // Create new view (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context)
                .inflate(R.layout.item_add_pedido, parent, false);
        // Return a new holder instance
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    // - get element from dataset
    // - replace the contents of the view with that element
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Refeicao refeicao = refeicoes.get(position);

        // definir id
        holder.id = refeicao.getId();

        // definir preço unitário
        holder.preco_unitario = refeicao.getPreço();

        // definir atributos
        holder.vNome.setText(refeicao.getNome());
        final NumberFormat nb = NumberFormat.getCurrencyInstance(); // para operações com dinheiro
        holder.vPreço.setText(nb.format(refeicao.getPreço()));

        // Descricao
        holder.vDescricao.setText(refeicao.getDescricao());

        // Checkbox listenner
        final EditText quantidade_pedidos = holder.vQuantidade;
        final CheckBox selecionado = holder.vSelecionado;
        final Float preco_unitario = holder.preco_unitario;
        final TextView preco = holder.vPreço;
        final ViewHolder ref_holder = holder;
        holder.vSelecionado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selecionado.isChecked()) {
                    quantidade_pedidos.setText("1");
                    pedidos.add(new Pedido(refeicao));
                } else {
                    if (removeTodosPedidosDaLista(ref_holder.id)){
                        quantidade_pedidos.setText("");
                        preco.setText(nb.format(preco_unitario));
                    }
                }
            }
        });

        // EditText atualizar preço a partir da quantidade
        holder.vQuantidade.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER) && selecionado.isChecked()) {
                    try {
                        int quantidade = Integer.parseInt(quantidade_pedidos.getText().toString());
                        Float novoPreco = preco_unitario * quantidade;
                        preco.setText(nb.format(novoPreco));
                        long qtd_pedidos_add = contaPedidos(ref_holder.id);
                        if (quantidade > qtd_pedidos_add){
                            for(int i = 0; i < quantidade-qtd_pedidos_add; i++)
                                pedidos.add(new Pedido(refeicao));
                        }else if (quantidade < qtd_pedidos_add){
                            removePedidosDaLista(ref_holder.id,qtd_pedidos_add-quantidade);
                        }
                    }catch(NumberFormatException e) {
                        Log.e(TAG, "Erro na atualização do preço, valores inválidos");
                    }
                }
                return false;
            }
        });
    }

    /**
     * Conta quantos pedidos daquele id estão na lista
     * @param id
     * @return
     */
    private long contaPedidos(long id){
        int c = 0;
        for(Pedido pedido: pedidos){
            if (pedido.getRefeicao().getId() == id){
                c++;
            }
        }
        return c;
    }

    /**
     * Remove todos os pedidos da lista após checkbox ser desmarcado
     * @param id
     * @return
     */
    private boolean removeTodosPedidosDaLista(long id){
        for(Pedido pedido: pedidos){
            if (pedido.getRefeicao().getId() == id){
                pedidos.remove(pedido);
            }
        }
        return true;
    }

    /**
     * Remove uma quantidade de pedidos da lista de um certo id
     * @param id
     * @return
     */
    private boolean removePedidosDaLista(long id, long quantidade){
        long restante = quantidade;
        for (int i = 0; i < pedidos.size() && restante > 0; i++){
            if (pedidos.get(i).getRefeicao().getId() == id){
                pedidos.remove(i);
                restante--;
            }
        }
        return true;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return refeicoes.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {//implements View.OnLongClickListener {

        private int id;
        private Float preco_unitario;
        private CheckBox vSelecionado;
        private TextView vNome;
        private TextView vDescricao;
        private TextView vPreço;
        private EditText vQuantidade;

        public ViewHolder(View view) {
            super(view);
            this.vSelecionado = (CheckBox) view.findViewById(R.id.pedido_selecionado);
            this.vNome = (TextView) view.findViewById(R.id.nome_refeicao);
            this.vDescricao = (TextView) view.findViewById(R.id.descricao);
            this.vPreço = (TextView) view.findViewById(R.id.preço);
            this.vQuantidade = (EditText) view.findViewById(R.id.qtd_refeicoes);
        }
    }
}
