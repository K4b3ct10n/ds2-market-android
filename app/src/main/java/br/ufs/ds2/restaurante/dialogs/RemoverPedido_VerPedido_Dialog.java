package br.ufs.ds2.restaurante.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import br.ufs.ds2.restaurante.adapters.Ver_Pedidos_ComandaAdapter;
import br.ufs.ds2.restaurante.interfaces.ExitDialog;
import br.ufs.ds2.restaurante.interfaces.Ver_Pedido_ExitDialog;

/**
 * Created by Junior on 27/08/2015.
 */
public class RemoverPedido_VerPedido_Dialog extends DialogFragment {

    private static final String TAG = "RemoverPedidoDialog";
    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static RemoverPedido_VerPedido_Dialog newInstance(int pk, int position) {
        RemoverPedido_VerPedido_Dialog dialog = new RemoverPedido_VerPedido_Dialog();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("pk", Integer.toString(pk));
        args.putInt("position", position);
        dialog.setArguments(args);
        return dialog;
    }

    public RemoverPedido_VerPedido_Dialog(){
        setArguments(getArguments());
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Use the Builder class for convenient dialog construction
        return new AlertDialog.Builder(getActivity())
                .setMessage("Deseja remover este pedido?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Ver_Pedido_ExitDialog activity = (Ver_Pedido_ExitDialog) getActivity();
                        activity.onFinishEditDialog(getArguments().getString("pk"),getArguments().getInt("position"));
                        dismiss();
                        /*Fragment targetFragment = getTargetFragment();
                        Intent intent = new Intent();
                        intent.putExtra("pk",getArguments().getString("pk"));
                        if (targetFragment != null) {
                            targetFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                        }*/
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                })
                .create();
    }
}
