package br.ufs.ds2.restaurante.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.controllers.AppController;

import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class CadastrarFornecedor extends AppCompatActivity {
    static String URL_CREATE_FORNECEDOR = "https://rest-dsdois.rhcloud.com/create_fornecedor.php?";
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_fornecedor);
        toolbar = (Toolbar) findViewById(R.id.toolbar_principal);
        toolbar.setTitle("Cadastrar Fornecedor");
        setSupportActionBar(toolbar);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void cadastrarFornecedor(String url){
        Log.e("CADASTRAR FORNECEDOR: ", url);

        StringRequest req = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if(response.contains("ok")){
                            Log.e("HttpClient", "success! response: " + response);
                            Toast t = Toast.makeText(getApplicationContext(),"Fornecedor cadastrado com sucesso.",Toast.LENGTH_SHORT);
                            t.show();
                            Intent intent = new Intent(getApplicationContext(),FornecedorActivity.class);
                            startActivity(intent);
                        }else{
                            Toast t = Toast.makeText(getApplicationContext(),"ERRO: Tente outra vez!",Toast.LENGTH_LONG);
                            t.show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("HttpClient", "error: " + error.toString());
                        Toast t = Toast.makeText(getApplicationContext(),"ERRO: Tente outra vez!",Toast.LENGTH_LONG);
                        t.show();
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req);
    }

    public void cadastrar(View v) throws URISyntaxException {
        EditText nome = (EditText) findViewById(R.id.et_forn_nome);
        EditText endereco = (EditText) findViewById(R.id.et_forn_endereco);
        EditText cnpj = (EditText) findViewById(R.id.et_forn_cnpj);
        EditText telefone = (EditText) findViewById(R.id.et_forn_telefone);
        URIBuilder uri = new URIBuilder(URL_CREATE_FORNECEDOR);
        uri.addParameter("nome",nome.getText().toString())
                .addParameter("endereco",endereco.getText().toString())
                .addParameter("cnpj",cnpj.getText().toString())
                .addParameter("telefone", telefone.getText().toString())
                .build();
        String url = uri.toString();
        cadastrarFornecedor(url);
    }
}
