package br.ufs.ds2.restaurante.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.controllers.AppController;

import org.apache.http.client.utils.URIBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class  CadastrarFuncionarioActivity extends AppCompatActivity {
    private Toolbar toolbarFuncionario;
    static String URL_CREATE_FUNCIONARIO = "http://rest-dsdois.rhcloud.com/create_garcom.php?";
    static String URL_CREATE_GARCOM = "http://rest-dsdois.rhcloud.com/create_garcom.php?";
    static String URL_CREATE_GERENTE = "http://rest-dsdois.rhcloud.com/create_gerente.php?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_funcionario);
        toolbarFuncionario = (Toolbar) findViewById(R.id.toolbar_principal);
        toolbarFuncionario.setTitle("Cadastrar Funcionário");
        setSupportActionBar(toolbarFuncionario);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void criarFuncionario(String url){
        //url = url.replaceAll("\\s+","_");
        Log.e("criarFuncionario","URL: "+url);
        StringRequest req = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("ok")){
                            Log.e("HttpClient", "success! response: " + response);
                            Toast t = Toast.makeText(getApplicationContext(),"FUNCIONÁRIO CADASTRADO COM SUCESSO",Toast.LENGTH_SHORT);
                            t.show();
                            Intent intent = new Intent(getApplicationContext(),FuncionarioActivity.class);
                            startActivity(intent);
                        }else{
                            Toast t = Toast.makeText(getApplicationContext(),"ERRO: Tente outra vez!",Toast.LENGTH_LONG);
                            t.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("HttpClient", "error: " + error.toString());
                        Toast t = Toast.makeText(getApplicationContext(),"ERRO, TENTE OUTRA VEZ.",Toast.LENGTH_LONG);
                        t.show();
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req);
    }

    public void cadastrar(View v) throws UnsupportedEncodingException, URISyntaxException {
        EditText nome = (EditText) findViewById(R.id.nome_funcionario);
        EditText cpf = (EditText) findViewById(R.id.cpf_funcionario);
        EditText endereco = (EditText) findViewById(R.id.endereco_funcionario);
        EditText telefone = (EditText) findViewById(R.id.telefone_funcionario);
        EditText salario = (EditText) findViewById(R.id.salario_funcionario);
        EditText dataAdmissao = (EditText) findViewById(R.id.dataAdmissao_funcionario);
        EditText cargo = (EditText) findViewById(R.id.cargo_funcionario);
        String tipo = cargo.getText().toString().toLowerCase();
        if (tipo.equals("gerente")){ URL_CREATE_FUNCIONARIO = URL_CREATE_GERENTE; }
        URIBuilder uri = new URIBuilder(URL_CREATE_FUNCIONARIO);
        uri.addParameter("nome",nome.getText().toString())
                .addParameter("endereco",endereco.getText().toString())
                .addParameter("telefone",telefone.getText().toString())
                .addParameter("cpf",cpf.getText().toString())
                .addParameter("salario", salario.getText().toString())
                .addParameter("dataDeAdmissao",dataAdmissao.getText().toString())
                .build();
        String url = uri.toString();
        criarFuncionario(url);
    }
}
