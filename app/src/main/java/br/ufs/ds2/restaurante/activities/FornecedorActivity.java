package br.ufs.ds2.restaurante.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.adapters.AdapterFornecedor;
import br.ufs.ds2.restaurante.controllers.AppController;
import br.ufs.ds2.restaurante.models.Fornecedor;

public class FornecedorActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
    private List<Fornecedor> lista_fornecedor;
    private RecyclerView rv_fornecedor;
    private RecyclerView.Adapter rv_adapter;
    private RecyclerView.LayoutManager rv_lm;
    private Toolbar toolbar;

    static String URL_LIST_FORNECEDOR = "http://rest-dsdois.rhcloud.com/read_fornecedor.php";
    static String URL_DELETE_FORNECEDOR = "http://rest-dsdois.rhcloud.com/delete_fornecedor.php?cnpj=";

    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fornecedor);
        toolbar = (Toolbar) findViewById(R.id.toolbar_principal);
        toolbar.setTitle("Fornecedores");
        setSupportActionBar(toolbar);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.sp_fornecedor);
        swipeRefreshLayout.setOnRefreshListener(this);
        lista_fornecedor = new ArrayList<Fornecedor>();
        rv_fornecedor = (RecyclerView) findViewById(R.id.recycler_view_fornecedor);
        rv_fornecedor.setHasFixedSize(true);
        rv_adapter = new AdapterFornecedor(lista_fornecedor,getApplicationContext());
        rv_lm = new LinearLayoutManager(this);
        rv_fornecedor.setLayoutManager(rv_lm);
        getFornecedores();
        rv_fornecedor.setAdapter(rv_adapter);


    }

    @Override
    public void onRefresh() {
        lista_fornecedor.clear();
        rv_adapter.notifyDataSetChanged();
        getFornecedores();
        swipeRefreshLayout.setRefreshing(false);
    }

    public void excluirFornecedor(String id, final int position){
        Log.e("excluirFornecedor","URL: "+URL_DELETE_FORNECEDOR+id);
        StringRequest req = new StringRequest(Request.Method.GET, URL_DELETE_FORNECEDOR+id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HttpClient", "success! response: " + response);
                        if (response.contains("ok")){
                            Toast t = Toast.makeText(getApplicationContext(),"FORNECEDOR EXCLUÍDO COM SUCESSO",Toast.LENGTH_SHORT);
                            t.show();
                            lista_fornecedor.remove(position);
                            rv_adapter.notifyItemRemoved(position);
                            for(int i=position; i < lista_fornecedor.size(); i++){
                                rv_adapter.notifyItemChanged(i);
                            }
                        }else{
                            Toast t = Toast.makeText(getApplicationContext(),"ESSE FORNECEDOR NÃO PODE SER EXCLUÍDO NO MOMENTO!",Toast.LENGTH_SHORT);
                            t.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("HttpClient", "error: " + error.toString());
                        Toast t = Toast.makeText(getApplicationContext(),"ERRO, TENTE OUTRA VEZ.",Toast.LENGTH_SHORT);
                        t.show();
                    }
                })
        {
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req);
    }

    public void getFornecedores(){
        JsonObjectRequest newsReq = new JsonObjectRequest(Request.Method.GET, URL_LIST_FORNECEDOR,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("getFornecedores ", response.toString());
                        loadFornecedores(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("getFornecedores ", "Erro: " + error.getMessage());
            }
        }) {
            @Override
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(newsReq);
    }

    public void loadFornecedores(JSONObject response){
        try {
            JSONArray perguntas = response.getJSONArray("fornecedores");
            for(int i=0; i < perguntas.length(); i++){
                JSONObject pg = perguntas.getJSONObject(i);
                Fornecedor fornecedor = new Fornecedor(pg.getString("nome"),pg.getString("endereco"),
                        pg.getString("cnpj"),pg.getString("telefone"));
                lista_fornecedor.add(fornecedor);
                rv_adapter.notifyItemInserted(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onContextItemSelected(MenuItem item) {
        int position = item.getOrder();
        switch (item.getItemId()) {
            case R.id.call:
                //Toast.makeText(getApplicationContext(),"ID: " + String.valueOf(item.getOrder()) ,Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(),AlterarFornecedorActivity.class);
                intent.putExtra("nome",lista_fornecedor.get(position).getNome());
                intent.putExtra("endereco",lista_fornecedor.get(position).getEndereco());
                intent.putExtra("cnpj",lista_fornecedor.get(position).getCnpj());
                intent.putExtra("telefone",lista_fornecedor.get(position).getTelefone());
                startActivity(intent);
                break;
            case R.id.msg:
                excluirFornecedor(lista_fornecedor.get(position).getCnpj(),position);
                break;
        }
        return super.onContextItemSelected(item);
    }
}
