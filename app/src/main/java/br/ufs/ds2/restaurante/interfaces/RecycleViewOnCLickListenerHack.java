package br.ufs.ds2.restaurante.interfaces;

import android.view.View;

/**
 * Created by Breno Cruz on 12/11/2015.
 */
public interface RecycleViewOnCLickListenerHack {
    void onClickListener(View view, int position);
    void onLongPressClickListener(View view, int position);

}
