package br.ufs.ds2.restaurante.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.OnAccountsUpdateListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import br.ufs.ds2.restaurante.R;


public class MenuActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private AccountManager accountManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        // Getting gerenciador de contas
        accountManager = AccountManager.get(getApplicationContext());
        // AccountManager listenner for logout
        // Listenner for logout
        OnAccountsUpdateListener accountsListener = new OnAccountsUpdateListener() {
            @Override
            public void onAccountsUpdated(Account[] accounts) {
                Account[] myAccounts = AccountManager.get(getApplicationContext()).getAccountsByType(LoginActivity.ACCOUNT_MANAGER_TYPE);
                if (myAccounts.length == 0) {
                    // The account has been deleted
                    Intent i = new Intent(MenuActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        };
        accountManager.addOnAccountsUpdatedListener(accountsListener, null, true);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        
        //Button btnModGarcom = (Button) findViewById(R.id.btnModuloGarcom);

        /*btnModGarcom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuActivity.this, GarcomActivity.class);
                startActivity(i);
            }
        });*/

        Button btnModRelatorios = (Button) findViewById(R.id.btnModuloRelatorios);

        btnModRelatorios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /*Intent i = new Intent(MenuActivity.this, RelatorioActivity.class);
                startActivity(i);*/

                Intent i = new Intent(MenuActivity.this, DataActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch(item.getItemId()){
            case R.id.action_exit:
                Account[] myAccounts = AccountManager.get(getApplicationContext()).getAccountsByType(LoginActivity.ACCOUNT_MANAGER_TYPE);
                accountManager.removeAccount(myAccounts[0], null, null);
                break;
        }
        return true;
    }

    public void moduloGerente(View v){
        Intent i = new Intent(MenuActivity.this, FuncionarioActivity.class);
        startActivity(i);
    }
}
