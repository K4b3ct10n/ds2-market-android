package br.ufs.ds2.restaurante.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.controllers.AppController;
import br.ufs.ds2.restaurante.models.Funcionário;
import br.ufs.ds2.restaurante.models.Garçom;
import br.ufs.ds2.restaurante.models.Gerente;
import br.ufs.ds2.restaurante.settings.RestauranteSettings;


public class LoginActivity extends Activity {

    private ArrayList<Funcionário> funcionarios;

    // Shared Preferences
    private AccountManager accountManager;
    // Constant to refer to Account type in AccountManager class
    protected static final String ACCOUNT_MANAGER_TYPE = "br.ufs.ds2.restaurante.user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Account Manager
        accountManager = AccountManager.get(getApplicationContext());
        final Account[] accounts = accountManager.getAccountsByType(ACCOUNT_MANAGER_TYPE);

        // Checa se usuário está logado
        if (accounts.length != 0) {
            if (accounts[0].name.equals("garçom")){
                Intent intent = new Intent(LoginActivity.this, GarcomActivity.class);
                startActivity(intent);
                finish();
            }else{
                Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        }

        funcionarios = new ArrayList<>();
        carregarFuncionarios();
        final EditText cpf = (EditText) findViewById(R.id.cpf);

        Button loginBtn = (Button) findViewById(R.id.btnLogin);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(Funcionário funcionário : funcionarios){
                    if(funcionário.getCpf().equals(cpf.getText().toString()) && funcionário.getTipo() == 0){ // Caso usuário seja Garçom
                        // Faz login no sistema como Garçom
                        Account account = new Account("garçom",ACCOUNT_MANAGER_TYPE);
                        Bundle bundle = new Bundle(); // armazena dados do funcionario logado
                        bundle.putInt("tipo", 0); //funcionario
                        bundle.putString("nome", funcionário.getNome());
                        bundle.putString("cpf",funcionário.getCpf());
                        bundle.putString("endereco",funcionário.getEndereço());
                        bundle.putString("telefone", funcionário.getTelefone());
                        bundle.putFloat("salario", funcionário.getSalario());

                        // Salva perfil logado no Cache
                        SharedPreferences settings = getSharedPreferences("garcom", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("cpf", funcionário.getCpf());
                        editor.commit();

                        accountManager.addAccountExplicitly(account,null,bundle);

                        // Segue para Activity do Garçom
                        Intent i = new Intent(LoginActivity.this, GarcomActivity.class);
                        startActivity(i);
                        finish();
                        return;
                    }else if(funcionário.getCpf().equals(cpf.getText().toString()) && funcionário.getTipo() == 1){ // Caso usuário seja Gerente
                        // Faz login no sistema como Gerente
                        Account account = new Account("gerente",ACCOUNT_MANAGER_TYPE);
                        Bundle bundle = new Bundle(); // armazena dados do funcionario logado
                        bundle.putInt("tipo", 1); // gerente
                        bundle.putString("nome", funcionário.getNome());
                        bundle.putString("cpf",funcionário.getCpf());
                        bundle.putString("endereco",funcionário.getEndereço());
                        bundle.putString("telefone",funcionário.getTelefone());
                        bundle.putFloat("salario", funcionário.getSalario());

                        // Salva perfil logado no Cache
                        SharedPreferences settings = getSharedPreferences("cpf_cliente", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("silentMode", funcionário.getCpf());
                        editor.commit();

                        accountManager.addAccountExplicitly(account,null,bundle);

                        // Segue para Activity do Garçom
                        Intent i = new Intent(LoginActivity.this, MenuActivity.class);
                        startActivity(i);
                        finish();
                        return;
                    }
                }
                Toast.makeText(getApplicationContext(),"CPF Inválido",Toast.LENGTH_LONG).show();
                return;
            }
        });
    }

    /**
     * Carrega lista de funcionários do webservice para o Login
     */
    private void carregarFuncionarios(){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                RestauranteSettings.getFuncionarios(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray array = response.getJSONArray("funcionarios");
                    for (int i = 0; i < array.length(); i++){
                        JSONObject jobj = array.getJSONObject(i);
                        if (jobj.getInt("tipo") == 0){
                            funcionarios.add(new Garçom(jobj.getString("nome"),jobj.getString("cpf"),
                                    jobj.getString("endereco"),jobj.getString("telefone"),(float) jobj.getDouble("salario")));
                        }else{
                            funcionarios.add(new Gerente(jobj.getString("nome"),jobj.getString("cpf"),
                                    jobj.getString("endereco"),jobj.getString("telefone"),(float) jobj.getDouble("salario")));
                        }
                    }
                }catch (JSONException e) {
                    Toast.makeText(getApplicationContext(),"Erro ao receber informações dos funcionários",Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Erro ao receber informações dos funcionários",Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
