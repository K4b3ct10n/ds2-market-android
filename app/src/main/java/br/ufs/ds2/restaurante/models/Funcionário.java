package br.ufs.ds2.restaurante.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Junior on 11/11/2015.
 */
public abstract class Funcionário implements Parcelable {

    private String nome;
    private String cpf;
    private String endereço;
    private String telefone;
    private Float salario;
    private Date dataAdmissao;
    private Date DataDemissao;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEndereço() {
        return endereço;
    }

    public void setEndereço(String endereço) {
        this.endereço = endereço;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Float getSalario() {
        return salario;
    }

    public void setSalario(Float salario) {
        this.salario = salario;
    }

    public Date getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(Date dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public Date getDataDemissao() {
        return DataDemissao;
    }

    public void setDataDemissao(Date dataDemissao) {
        DataDemissao = dataDemissao;
    }

    public abstract int getTipo();
}
