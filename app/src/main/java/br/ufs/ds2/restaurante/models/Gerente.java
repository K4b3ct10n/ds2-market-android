package br.ufs.ds2.restaurante.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Junior on 28/11/2015.
 */
public class Gerente extends Funcionário implements Parcelable{

    public Gerente(String nome, String cpf, String endereco, String telefone, float salario) {
        super.setNome(nome);
        super.setCpf(cpf);
        super.setEndereço(endereco);
        super.setTelefone(telefone);
        super.setSalario(salario);
    }

    public Gerente(String nome, String cpf, String endereco, String telefone) {
        super.setNome(nome);
        super.setCpf(cpf);
        super.setEndereço(endereco);
        super.setTelefone(telefone);
    }

    protected Gerente(Parcel in) {
        super.setNome(in.readString());
        super.setCpf(in.readString());
        super.setEndereço(in.readString());
        super.setTelefone(in.readString());
        super.setSalario(in.readFloat());
    }

    public static final Creator<Gerente> CREATOR = new Creator<Gerente>() {
        @Override
        public Gerente createFromParcel(Parcel in) {
            return new Gerente(in);
        }

        @Override
        public Gerente[] newArray(int size) {
            return new Gerente[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(super.getNome());
        dest.writeString(super.getCpf());
        dest.writeString(super.getEndereço());
        dest.writeString(super.getTelefone());
        dest.writeFloat(super.getSalario());
    }

    @Override
    public int getTipo() {
        return 1;
    }
}
