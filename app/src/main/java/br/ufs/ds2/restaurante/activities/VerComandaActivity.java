package br.ufs.ds2.restaurante.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.adapters.Ver_Pedidos_ComandaAdapter;
import br.ufs.ds2.restaurante.controllers.AppController;
import br.ufs.ds2.restaurante.interfaces.Ver_Pedido_ExitDialog;
import br.ufs.ds2.restaurante.models.Comanda;
import br.ufs.ds2.restaurante.models.Pedido;
import br.ufs.ds2.restaurante.settings.RestauranteSettings;

/**
 * Created by Junior on 26/11/2015.
 */
public class VerComandaActivity extends AppCompatActivity implements Ver_Pedido_ExitDialog {

    private Toolbar toolbar;
    private Ver_Pedidos_ComandaAdapter adapter;
    private ArrayList<Pedido> pedidos;
    private TextView num_comanda;
    private TextView vValor_total;
    private EditText cliente;
    private Spinner mesa;
    private float valor_total;
    private boolean lista_pedidos = false; // verifica se lista de pedidos foi alterada
    private ArrayList<Pedido> pedidosNovos = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_comanda);

        // ActionBar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Getting comanda from intent
        Comanda comanda = getIntent().getParcelableExtra("Comanda");

        //RecyclerView
        pedidos = new ArrayList<>();
        pedidos.addAll(comanda.getPedidos());
        adapter = new Ver_Pedidos_ComandaAdapter(pedidos,getSupportFragmentManager());
        RecyclerView pedidosList = (RecyclerView) findViewById(R.id.lista_pedidos);
        pedidosList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        pedidosList.setAdapter(adapter);
        pedidosList.setHasFixedSize(true);

        num_comanda = (TextView) findViewById(R.id.numero_comanda);
        num_comanda.setText(String.valueOf(comanda.getId()));
        cliente = (EditText) findViewById(R.id.nome_cliente);
        cliente.setText(comanda.getCliente());

        mesa = (Spinner) findViewById(R.id.spinner_num_mesa);
        Integer[] spinnerArray = {1,2,3,4,5,6,7,8,9};
        ArrayAdapter<Integer> spinnerArrayAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mesa.setAdapter(spinnerArrayAdapter);
        mesa.setSelection(comanda.getMesa() - 1);
        vValor_total = (TextView) findViewById(R.id.valor_total_comanda);
        vValor_total.setText(calculaValorAtual());

    }

    /**
     * Atualiza valor atual da comanda na tela
     * @return
     */
    private String calculaValorAtual() {
        NumberFormat nb = NumberFormat.getCurrencyInstance();
        for(Pedido pedido: pedidos){
            valor_total += pedido.getRefeicao().getPreço();
        }
        return nb.format((double) valor_total);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ver_comanda, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_add: // Adicionar Pedido
                Intent i = new Intent(this,AddPedidosActivity.class);
                i.putExtra("id",num_comanda.getText().toString());
                startActivityForResult(i, 1);
                break;
            case R.id.action_pay: // Pagar comanda
                AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(VerComandaActivity.this);
                myAlertDialog.setTitle("Pagamento da Comanda");
                myAlertDialog.setMessage("Deseja pagar a comanda e fechá-la?");
                final int numero_comanda = Integer.parseInt(num_comanda.getText().toString());
                myAlertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        pagarComanda(numero_comanda, valor_total);
                        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(VerComandaActivity.this);
                    }
                });
                myAlertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        // do something when the Cancel button is clicked
                    }
                });
                myAlertDialog.show();
                break;
            case R.id.action_confirm: // Alterar dados da comanda
                alterarComanda(cliente.getText().toString(),(int) mesa.getSelectedItem(),num_comanda.getText().toString());
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            if(resultCode == RESULT_OK) {
                Parcelable[] pedidos = data.getParcelableArrayExtra("Pedidos");
                pedidosNovos.clear();
                for (Parcelable pedido : pedidos) {
                    setPedidosNovos((Pedido) pedido);
                    valor_total += ((Pedido) pedido).getRefeicao().getPreço();
                }
                vValor_total.setText(NumberFormat.getCurrencyInstance().format(valor_total));
                adapter.notifyDataSetChanged();
                lista_pedidos = true;
            }
        }
    }

    private void alterarComanda(String cliente, int mesa, String num_comanda) {
        final ProgressDialog progressDialog = new ProgressDialog(VerComandaActivity.this);
        progressDialog.setMessage("Cadastrando comanda...");
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.GET,
                RestauranteSettings.getAlterarClienteMesaComanda(cliente, String.valueOf(mesa), num_comanda),new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (lista_pedidos)
                    cadastrarPedidos(pedidosNovos, progressDialog);
                lista_pedidos = false;
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Ocorreu um erro ao fechar a comanda. Tente novamente", Toast.LENGTH_LONG).show();
            }
        });
        // Adicionando requisição a fila de requisições
        AppController.getInstance().addToRequestQueue(request);
    }

    /**
     * Cadastrar novos pedidos no webservice
     * @param pedidos
     * @param progressDialog
     */
    private void cadastrarPedidos(ArrayList<Pedido> pedidos, final ProgressDialog progressDialog) {
        try {
            JSONArray jsonArray = new JSONArray();
            for (Pedido pedido: pedidos) {
                JSONObject jobj = new JSONObject();
                jobj.put("id_comanda",num_comanda.getText().toString());
                jobj.put("id_refeicao",String.valueOf(pedido.getRefeicao().getId()));
                jsonArray.put(jobj);
            }
            JSONObject jobj = new JSONObject();
            jobj.put("pedidos", jsonArray);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                    RestauranteSettings.getCriar_pedidos(),jobj.toString()
                    , new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(getApplicationContext(), "Comanda alterada com sucesso", Toast.LENGTH_LONG).show();
                    progressDialog.hide();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
                }
            });
            // Adicionando requisição a fila de requisições
            AppController.getInstance().addToRequestQueue(request);
        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Paga a comanda e define-a como fechada no webservice
     * @param numero_comanda
     */
    private void pagarComanda(int numero_comanda, float valor_total) {
        StringRequest request = new StringRequest(Request.Method.GET,
                RestauranteSettings.getPagar_comanda(String.valueOf(numero_comanda), String.valueOf(valor_total)),
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getApplicationContext(), "Comanda fechada com sucesso", Toast.LENGTH_LONG).show();
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Ocorreu um erro ao fechar a comanda. Tente novamente", Toast.LENGTH_LONG).show();
            }
        });
        // Adicionando requisição a fila de requisições
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public void onFinishEditDialog(String id, int position) {
        removerPedido(id, position);
    }

    /**
     * Remove pedido do sistema
     * @param id
     * @param position
     */
    private void removerPedido(String id, final int position){
        StringRequest request = new StringRequest(Request.Method.GET,
                RestauranteSettings.getRemover_pedido(id),new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.equals("ok")){
                    Toast.makeText(getApplicationContext(), "Pedido removido com sucesso", Toast.LENGTH_LONG).show();
                    pedidos.remove(position);
                    adapter.notifyDataSetChanged();
                }else{
                    Toast.makeText(getApplicationContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
            }
        });
        // Adicionando requisição a fila de requisições
        AppController.getInstance().addToRequestQueue(request);
    }

    public void setPedidos(Pedido pedidos) {
        this.pedidos.add(pedidos);
    }

    public void setPedidosNovos(Pedido pedidosNovos) {
        this.pedidos.add(pedidosNovos);
        this.pedidosNovos.add(pedidosNovos);
    }
}
