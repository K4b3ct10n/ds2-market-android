package br.ufs.ds2.restaurante.adapters;


import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.ufs.ds2.restaurante.R;

/**
 * Created by Junior on 11/11/2015.
 */
public class GanhosAdapter extends RecyclerView.Adapter<GanhosAdapter.ViewHolder> {

    /*
   * Classe responsável por fazer referências aos dados ganhos em um período e alimentar os fragmentos de consultas
   * */
    private List<Float> ganhoList; //Lista com os dados ganhos
    private static FragmentManager fragManager;

    public GanhosAdapter(List<Float> comments, FragmentManager fragManager) {
        this.ganhoList = comments;
        GanhosAdapter.fragManager = fragManager;
    }

    // Create new view (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_ganho, parent, false);
        // Return a new holder instance
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    // - get element from dataset
    // - replace the contents of the view with that element
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Float ganhos = ganhoList.get(position);
        // Set comment attributes
        holder.vGanhos.setText(ganhos.toString());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return ganhoList.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        private TextView vGanhos;

        public ViewHolder(View view) {
            super(view);
            this.vGanhos = (TextView) view.findViewById(R.id.total_ganho);
            // Attach a onLongClickListener to the entire row view for exclude comment Dialog
            view.setOnLongClickListener(this);
        }

        // Shows exclude comment dialog
        @Override
        public boolean onLongClick(View v) {

            return true;
        }
    }
}
