package br.ufs.ds2.restaurante.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.ufs.ds2.restaurante.R;
import br.ufs.ds2.restaurante.activities.VerComandaActivity;
import br.ufs.ds2.restaurante.adapters.PedidosAdapter;
import br.ufs.ds2.restaurante.controllers.AppController;
import br.ufs.ds2.restaurante.interfaces.RecycleViewOnCLickListenerHack;
import br.ufs.ds2.restaurante.models.Comanda;
import br.ufs.ds2.restaurante.models.Garçom;
import br.ufs.ds2.restaurante.models.Pedido;
import br.ufs.ds2.restaurante.models.Refeicao;
import br.ufs.ds2.restaurante.settings.RestauranteSettings;

/**
 * Created by Junior on 10/11/2015.
 */
public class PedidosFragment extends Fragment implements RecycleViewOnCLickListenerHack{

    private RecyclerView mRecyclerView;
    public static List<Pedido> mList;
    private List<Comanda> mListComanda;
    private PedidosAdapter mPedidosAdapter;

    private static int count;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pedidos, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //fim da lista na tela
            }
        });

        mRecyclerView.addOnItemTouchListener(new RecycleViewTouchListener(getActivity(), mRecyclerView, this));

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);
        carregarPedidosAtivos();
        //mList = preencherLista(10);
        //ordenarLista(mList);
        mPedidosAdapter = new PedidosAdapter(getActivity(), mList);
        //mPedidosAdapter.setRecycleViewOnCLickListenerHack(this);
        mRecyclerView.setAdapter(mPedidosAdapter);

        return view;
    }

    private List<Pedido> preencherLista(int quantia){
        List<Pedido> l = new ArrayList<>();

        for(int i = quantia; i > 0; i--)
            l.add(new Pedido(i, i, new Refeicao(1,"Filé ao Frango",9), -1));
        return l;
    }

    private void ordenarLista(List<Pedido> list){
        Pedido aux = new Pedido(-1, -1, null, -1);

        for(int i = 0; i < list.size(); i++){
            for(int j = i + 1; j < list.size(); j++){
                if(list.get(j).getNumPedido() < list.get(i).getNumPedido()){
                    aux.clone(list.get(i));
                    list.get(i).clone(list.get(j));
                    list.get(j).clone(aux);
                }
            }
        }
    }

    @Override
    public void onClickListener(View view, int position) {
        Intent intent = new Intent(getContext(), VerComandaActivity.class);
        Comanda comanda = null;
        for (Comanda c : mListComanda) {
            if (c.getId() == mList.get(position).getIdComanda()) {
                comanda = c;
                break;
            }
        }

        intent.putExtra("Comanda", comanda);
        getContext().startActivity(intent);
    }

    @Override
    public void onLongPressClickListener(View view, final int position) {
        final CharSequence colors[] = new CharSequence[] {"Visualizar Comanda", "Excluir Pedido"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("O que deseja fazer no pedido número " + mList.get(position).getNumPedido() + "?");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
//                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
//                        LayoutInflater inflater = getActivity().getLayoutInflater();
//
//                        View v = inflater.inflate(R.layout.dialog_view_pedido, null);
//                        TextView txtNomePedido = (TextView) v.findViewById(R.id.nomePedido);
//                        txtNomePedido.setText("Pedido Número "+mList.get(position).getNumPedido());
//                        TextView txtNumPedido = (TextView) v.findViewById(R.id.numPedido);
//                        txtNumPedido.setText("Número do Pedido: "+mList.get(position).getNumPedido()+"");
//                        TextView txtNomeRefeicao = (TextView) v.findViewById(R.id.nomeRefeicao);
//                        txtNomeRefeicao.setText("Nome da Refeição: "+mList.get(position).getRefeicao().getNome());
//
//                        alertDialog.setView(v);
//
//                        /* When positive (yes/ok) is clicked */
//                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.cancel(); // Your custom code
//                            }
//                        });
//
//                        alertDialog.show();

                        Intent intent = new Intent(getContext(), VerComandaActivity.class);
                        Comanda comanda = null;
                        for (Comanda c : mListComanda) {
                            if (c.getId() == mList.get(position).getIdComanda()) {
                                comanda = c;
                                break;
                            }
                        }

                        intent.putExtra("Comanda", comanda);
                        getContext().startActivity(intent);
                        break;
                    case 1:
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Confirma Exlusão?")
                                .setCancelable(false)
                                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    StringRequest request = new StringRequest(Request.Method.GET,
                                            RestauranteSettings.getRemover_pedido(mList.get(position).getId()+""),new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            if (response.equals("ok")){
                                                Toast.makeText(getActivity(), "Pedido Número " + mList.get(position).getNumPedido() + " removido com Sucesso!", Toast.LENGTH_SHORT).show();
                                                mPedidosAdapter.removeItemList(position);
                                            }else{
                                                Toast.makeText(getContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Toast.makeText(getContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                    // Adicionando requisição a fila de requisições
                                    AppController.getInstance().addToRequestQueue(request);

                                    }
                                })
                                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.setIcon(android.R.drawable.ic_dialog_alert);
                        alert.show();
                    break;
                }
            }
        });
        builder.show();
    }

    private static class RecycleViewTouchListener implements RecyclerView.OnItemTouchListener{
        private Context mContext;
        private GestureDetector mGestureDetector;
        private RecycleViewOnCLickListenerHack mRecycleViewOnCLickListenerHack;

        public RecycleViewTouchListener(Context c, final RecyclerView rv, RecycleViewOnCLickListenerHack rvoclh){
            mContext = c;
            mRecycleViewOnCLickListenerHack = rvoclh;

            mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);

                    View cv = rv.findChildViewUnder(e.getX(), e.getY());

                    if (cv != null && mRecycleViewOnCLickListenerHack != null){
                        mRecycleViewOnCLickListenerHack.onLongPressClickListener(cv, rv.getChildAdapterPosition(cv));
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    View cv = rv.findChildViewUnder(e.getX(), e.getY());

                    if (cv != null && mRecycleViewOnCLickListenerHack != null){
                        mRecycleViewOnCLickListenerHack.onClickListener(cv, rv.getChildAdapterPosition(cv));
                    }
                    return true;
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            mGestureDetector.onTouchEvent(e);
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {}

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}
    }


    @Override
    public void onResume() {
        super.onResume();
        mPedidosAdapter = new PedidosAdapter(getActivity(), mList);
        mRecyclerView.setAdapter(mPedidosAdapter);
    }

    private void carregarPedidosAtivos() {
        mList = new ArrayList<>();
        mListComanda = new ArrayList<>();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                RestauranteSettings.getConsultar_Comandas(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        count = 0;
                        mList.clear();
                        mListComanda.clear();
                        try {
                            JSONArray jsonArray = response.getJSONArray("comandas");
                            for(int i = 0; i < jsonArray.length(); i++){
                                JSONObject comandaJson = (JSONObject) jsonArray.get(i);
                                if (comandaJson.getInt("ativa") == 1){
                                    if (comandaJson.has("pedidos")) {
                                        JSONArray pedidosArrayJson = comandaJson.getJSONArray("pedidos");
                                        List<Pedido> pedidos = new ArrayList<>();
                                        for (int j = 0; j < pedidosArrayJson.length(); j++) {
                                            JSONObject pedidoJson = (JSONObject) pedidosArrayJson.get(j);
                                            Refeicao refeicao = new Refeicao(Integer.parseInt(pedidoJson.getString("id_refeicao")),
                                                    pedidoJson.getString("nome"), pedidoJson.getString("descricao"), Float.parseFloat(pedidoJson.getString("preco")));
                                            Pedido pedido = new Pedido(Integer.parseInt(pedidoJson.getString("id_pedido")), ++count, refeicao, comandaJson.getInt("id_comanda"));
                                            pedidos.add(pedido);
                                           mList.add(pedido);
                                        }

                                        JSONObject garcomJson = comandaJson.getJSONObject("garcom");
                                        Garçom garcom = new Garçom(garcomJson.getString("nome"),garcomJson.getString("cpf"),
                                                garcomJson.getString("endereco"),garcomJson.getString("telefone"),(float) garcomJson.getDouble("salario"));
                                        Comanda comanda = new Comanda(comandaJson.getInt("id_comanda"),comandaJson.getString("cliente"),
                                                pedidos,garcom,(float) comandaJson.getDouble("valorTotal"),comandaJson.getInt("mesa"));
                                        mListComanda.add(comanda);
                                    }
                                }
                            }
                            Log.i("Script", "Tabamho da lista: " + mList.size());
                            ordenarLista(mList);
                            mPedidosAdapter.notifyDataSetChanged();
                          //  adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            Toast.makeText(getContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Ocorreu um erro. Tente novamente", Toast.LENGTH_LONG).show();
            }
        });
        // Adicionando requisição a fila de requisições
        AppController.getInstance().addToRequestQueue(request);
    }

}